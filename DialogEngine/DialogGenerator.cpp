/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "DialogGenerator.h"


DialogGenerator::DialogGenerator(string xml_file)
{
    _dialog = NULL;
    _prompt = NULL;
    _node = NULL;

	this->mDialog = NULL;
	this->TREE_COUNTER = 0;
    this->BuildDialogsFromFile(xml_file);
}

DialogGenerator::DialogGenerator(DialogGenerator& dummy) {
	// Private
    this->mDialog = dummy.mDialog;
}

DialogGenerator::~DialogGenerator()
{
	delete this->mDialog;
}

// Test Dialog without dependancies  
Dialog* DialogGenerator::RunDemo() {

	// DialogGenerator* pDialogGenerator = new DialogGenerator("SubDialogueDemo.txt");

	DialogGenerator* pDialogGenerator = new DialogGenerator("FlightScheduler.txt");

	// DialogGenerator* pDialogGenerator = new DialogGenerator("OrderCoffee.txt");

	return pDialogGenerator->GetDialog();
	
}

Dialog* DialogGenerator::GetDialog() {
	return this->mDialog;
}

void DialogGenerator::BuildDialogsFromFile(string xml_file) {
    // Read specification
    string XML_Text = "";
    vector<string> expressions;
    ReadSpecificationFile(xml_file, XML_Text, expressions, true);

    if (XML_Text == "") {
        cout << endl << "ERROR: No specification file found at : " << xml_file << endl;
        return;
    }

	// Parse XML
    using namespace rapidxml;
    xml_document<> doc;    // character type defaults to char
    doc.parse<0>((char*)XML_Text.c_str());    // 0 means default parse flags
    if (IS_ERROR(ParseXML(doc.first_node(), DIALOG), "Invalid XML, No Dialog Created", USER_SEVERE)) {

        // TODO: delete pseudodialogs map

        return; // No dialog is created
    }

    // Build Expressions
    this->BuildExpressions(expressions);    // Build Expressions
    this->mDialog->CompleteDialog();        // Complete Dialog
}

bool DialogGenerator::ParseXML(xml_node<> * node, XML_LEVEL level) {

    // Get First Node
    for ( ; node && node != node->next_sibling();
        node = node->next_sibling())
    {
        if (level == DIALOG) {
            // Check that node is a Dialog
            if (IS_ERROR(((string)node->name()).compare("dialog") == 0 || ((string)node->name()).compare("Dialog") == 0,
                "Expected <Dialog> but got " + (string)node->name(), USER_SEVERE)) {
                return false;
            }

            // Get dialog information
            string entry_prompt = "Welcome!", dialog_name="New Dialog";
            if(IS_NOT_ERROR(node->first_attribute("name") != NULL, "No Dialog Name Given", USER_CRITICAL)) {
                dialog_name = node->first_attribute("name")->value();
            }
            if(IS_NOT_ERROR(node->first_attribute("entry_prompt") != NULL, "No Entry Prompt Given", USER_CRITICAL)) {
                entry_prompt = node->first_attribute("entry_prompt")->value();
            }

            // Create Pseudo Dialog
            _dialog = new PseudoDialog(dialog_name, entry_prompt);

            // Build Prompts
            if (!ParseXML(node->first_node(), PROMPT)) {
                delete _dialog;
                _dialog = NULL;
                return false;
            }

            // Add Dialog to Dialog Map
            if (IS_ERROR(_dialog_map.find(dialog_name) == _dialog_map.end(),
                         "Dialog Redefined!", USER_SEVERE)) {
                delete _dialog;
                _dialog = NULL;
                return false;
            }
            this->_dialog_map[dialog_name] = _dialog;
        }
        else if (level == PROMPT) {
            // Check that node is a prompt
            if (IS_ERROR(((string)node->name()).compare("prompt") == 0 || ((string)node->name()).compare("Prompt") == 0,
                "Expected <Prompt> but got " + (string)node->name(), USER_SEVERE)) {
                return false;
            }

            // Get prompt information
            string promptName = "", prompt = "";
            promptName	= node->first_attribute("name")->value();
            prompt	    = node->first_attribute("prompt")->value();

            // Create new prompt
            _prompt = new PseudoPrompt(promptName, prompt);

            // Build Responses
            if (!ParseXML(node->first_node(), RESPONSE)) {
                delete _prompt;
                _prompt = NULL;
                return false;
            }

            // Add Prompt to Dialog
            if(!_dialog->AddPrompt(promptName, _prompt)) {
                delete _prompt;
                _prompt = NULL;
                return false;
            }
        }
        else if (level == RESPONSE) {
            // Check that node is a node
            if (IS_ERROR(((string)node->name()).compare("node") == 0 || ((string)node->name()).compare("Node") == 0,
                "Expected <Node> but got " + (string)node->name(), USER_SEVERE)) {
                return false;
            }

            // Get node information
            string ResponseValue = node->first_attribute("name")->value();

            // Add Response to Prompt
            _node = new PseudoNode(ResponseValue, "");
            if (!_prompt->AddResponse(ResponseValue, _node)) {
                delete _node;
                _node = NULL;
                return false;
            }
        }
    }

    return true;
}



//          fun <identifier>(<pattern1>) = <expression1>
//          | <identifier>(<pattern2>) = <expression2>
//          | ...
//          | <identifier>(<patternK>) = <expressionK>;
void DialogGenerator::BuildExpressions(vector<string> &expressions) {
    for (string expr : expressions) {
        REPORT_ERROR("Building Expression: " + expr);

        // Declare new tree vector
        vector<long long> NodeTreeVector;
        NodeTreeVector.push_back(++this->TREE_COUNTER);

        // Remove extra whitespace
        expr = reduce(expr);

        // Match Expression
//        regex expression_re("[(pe\\*\\()(c\\()(i\\()(spe'\\()(pfan\\()].*");
//        if (IS_FALSE(regex_match(expr, expression_re), "Invalid Expression!", CRITICAL)) {
//            continue;
//        }

        // Extract Dialog Name
        string dialog_name = expr.substr(0, expr.find_first_of(" \t"));
        expr.erase(0, dialog_name.size());
        expr = trim(expr);

        // Lookup Dialog Data
        map<string, PseudoDialog*>::iterator it = this->_dialog_map.find(dialog_name);
        if (IS_ERROR(it != this->_dialog_map.end(), "No data for dialog " + dialog_name, USER_CRITICAL)) {
            continue;
        }
        PseudoDialog * pDialog = it->second;

        // Create Dialog
        this->mDialog = new Dialog(pDialog->GetName(), pDialog->GetPrompt());
        REPORT_ERROR("Generator Created Dialog " + dialog_name, VERBOSE);

        // Extract Eval Strategy
        string eval = GetEvaluation(expr); // eval must not be empty
        if (IS_ERROR(eval.length() != 0, "No evaluation strategy!", USER_SEVERE)) {
            continue; // Dialog name does not exist!
        }

        // Create Root Node
        DialogNode* Root_Parent = this->mDialog->AddRootToDialog(eval,NodeTreeVector.at(NodeTreeVector.size() - 1));
        REPORT_ERROR("Generator Created Root Node of Type " + eval, VERBOSE);


        vector<DialogNode*> parent_nodes = {Root_Parent};

        // Split expression pattern matching
        vector<string> patterns;
        split(expr, "\\|\\s*", patterns);

        for (string pattern : patterns) {
            string pattern_eval = GetEvaluation(pattern); // eval must not be empty
            pattern.erase(0, pattern_eval.length()+1); // remove "("
            if (IS_ERROR(pattern_eval.compare(eval) == 0,
                         "Pattern Matching of parent evaluation strategy did not match!", USER_SEVERE)) {
                continue;
            }
            BuildNodeChildren(pattern, parent_nodes, pDialog, NodeTreeVector);
        }
    }
}

string DialogGenerator::GetEvaluation(string &expression) {
    vector<std::string> elems;
    string eval = "";

    split(expression, "\\(.*", elems); // elems size will be 0 or 1
    if (elems.size() == 1 && expression.length() > elems.at(0).length() ) {
         eval = elems.at(0);
         elems.clear();
//         expression.erase(0, eval.length()+1); // remove "("
    }

    return eval;
}

void DialogGenerator::BuildNodeChildren(string expression, vector<DialogNode*> ParentNodes,
                                        PseudoDialog * pDialog, vector<long long> tree_vector) {
    DialogNode* NewNode = NULL;
    vector<DialogNode*> NewParents;
    vector<string> elems, resps_vec;

    // Start Getting Nodes
    split(expression, ", ", elems); // Size at Least 1
//    if (elems.size() != 1) {
//        elems.clear();
//        split(expression, "\\)", elems); // Remove at Least 1 ")"
//    }

    // This is the next node to look at
    string Node = elems.at(0);

    bool CreatesSubDialog, EndsSubDialog;

    // Check if this node starts a sub-dialog
    string eval = GetEvaluation(Node);
    CreatesSubDialog = eval.length() > 0;        // Sub Dialog
    if (CreatesSubDialog) {
        expression.erase(0, eval.length()+1); // +1 for "("
        Node.erase(0, eval.length()+1); // +1 for "("
    }

    int number_closed = 0;
    // Check if this node ends a sub-dialog
    while (Node.at(Node.length() - 1) == ')') {
        if (elems.size() > 1) {
            number_closed++;
        }
        EndsSubDialog = true;
        Node.pop_back();
    }

    // Node cannot both start and end a subdialog!
    // If both, handle by attempting to exit only, (ignore eval)
    if (IS_NOT_ERROR(!(CreatesSubDialog && EndsSubDialog),
            "Exit and Eval cannot be properties of the same node! Will attempt Exit only! (Ignoring Eval)", USER_SEVERE))
    {
        // Check if declares a subdialog
        if (CreatesSubDialog) {
            tree_vector.push_back(++this->TREE_COUNTER); // new tree index
        }
    }

    // Check if exits a subdialog - Will not happen if a node is a subdialog
    // Set eval to exit if EndsSubDialog
    if (EndsSubDialog && IS_NOT_ERROR((tree_vector.size() > number_closed),
            "Attempted to exit master dialog during generation! (Ignoring Exit)", NORMAL))
    {
        eval = ""; // must not open a sub dialog
        for (int i = 0; i < number_closed; i++) {
            tree_vector.pop_back();
        }
    }

    // split node into prompt::responses
    split(Node, "::", resps_vec); // resps MUST have size 2
    if (IS_ERROR(resps_vec.size() == 2, "Improper Number of Responses!", ABORT)) {
         return; // next expr
    }
    // Lookup Prompt
    map<string, PseudoPrompt*>::iterator pPrompt_it = pDialog->PromptMap.find(resps_vec.at(0));
    if(IS_ERROR(pPrompt_it != pDialog->PromptMap.end(),
                "Could not find specified Prompt: " + resps_vec.at(0), USER_CRITICAL)) {
        return; // next expr
    }
    PseudoPrompt* pPrompt = pPrompt_it->second;
    string responses = resps_vec.at(1);
    resps_vec.clear();

    // Check if responses == _any
    if (responses.compare("_any") == 0) {
        map<string, PseudoNode*>::iterator pNode_it = pPrompt->mResponseMap.begin();
        for (; pNode_it != pPrompt->mResponseMap.end(); pNode_it++) {
            PseudoNode* pNode = pNode_it->second;
            NewNode = this->mDialog->AddNodeToDialog(ParentNodes,
                      pNode->GetValue(), "", pPrompt->GetPrompt(), // TODO: replace empty string with context
                      " (no details) ", tree_vector.at(tree_vector.size() - 1), eval, number_closed);
            NewParents.push_back(NewNode);
            REPORT_ERROR("Generator Added Node " + pNode->GetValue(), VERBOSE);

        }
    }
    else {
        // Split multiple responses
        split(responses, ":", resps_vec); // resps >= 1
        for (string response : resps_vec) {
            map<string, PseudoNode*>::iterator pNode_it = pPrompt->mResponseMap.find(response);
            if (IS_ERROR(pNode_it != pPrompt->mResponseMap.end(), "Resonse Not Found: " + response, USER_SEVERE)) {
                continue;
            }
            PseudoNode* pNode = pNode_it->second;
            NewNode = this->mDialog->AddNodeToDialog(ParentNodes,
                          pNode->GetValue(), "", pPrompt->GetPrompt(), // TODO: replace empty string with context
                          "details", tree_vector.at(tree_vector.size() - 1), eval, number_closed);
            NewParents.push_back(NewNode);
            REPORT_ERROR("Generator Added Node " + pNode->GetValue(), VERBOSE);
        }
        resps_vec.clear();
    }

    if (elems.size() > 1) {
        expression.erase(0, Node.length()+2); // +2 for ", "
        BuildNodeChildren(expression, NewParents, pDialog, tree_vector);
    }

    return;
}
