/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the project nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#pragma once

#include "DialogInclude.h"
#include "DialogNode.h"
#include "Staging/StagingData.h"
#include "NLPU/NLPUDictionary.h"

class Dialog
{
public:

	enum RESPONSE {
		FALSE,
		TRUE,
		Sub_Parent_Dialog_MisMatch
	};

private: 
    // EVERYTHING IS PRIVATE, only DialogGenerator and DialogStager can access member variables and functions
	friend class DialogGenerator;
	friend class DialogStager;

	// CONSTRUCTOR, DESTRUCTOR FUNCTIONS: ---------------------------------------------------------------
    Dialog(string dialog_name, string start_prompt, bool CreateDictionary = true);

	Dialog(Dialog& dialog);

	~Dialog();

	DialogNode* AddRootToDialog(
		string eval_type,
		long long treeID);

	// GENERATION FUNCTIONS: ----------------------------------------------------------------------------

	// Add Node
    DialogNode* AddNodeToDialog(vector<DialogNode *> nParentNodes,
        string nValue,
        string nContextGrammar,
        string nSolicitation,
        string nDetails,
        long long nTreeID,
        string nEvaluationMethod = "",
        int endsNDialogs = 0);

	bool CompleteDialog();

	size_t LabelAllBranches(DialogNode* node, int branchID, size_t numCalls = 0);

    vector<long long> RootLabelAllBranches(DialogNode* node, int &branchID);

	int searchKeysFor(string key);

	// Adds node to table of nodes for dialog
	bool AddNodeToDialogTable(DialogNode* node);

	bool RemoveIfEndNode(DialogNode* node);


	// SET, GET, IS FUNCTIONS: --------------------------------------------------------------------------
	string					GetStartPrompt();
	string					getAffirmResp();
	string					getNegResp();
	vector<DialogNode*>		GetStartNodes();

	// Data

    NLPUDictionary * mDictionary;

	typedef vector<DialogNode*> Nodes;

	vector<DialogNode*>					mStartNodes;
	map<string, Nodes*>					m_ALL_NODES_IN_DIALOG;
    map<string, vector<long long> >     m_NODE_NAME_2_BRANCHES;
	map<DialogNode*, DialogNode*>		m_END_POINTS;
    map<string, string>					m_DESC_STATEMENTS; // ? ? ? Do I really need this?
	map<long long, DialogNode::Method>	m_EVAL_LOOKUP;
	map<long long, DialogNode*>			m_SUBDIALOG_LOOKUP;
    map<long long, long long>			m_PARENT_TREE_LOOKUP;
	vector<string>						m_all_keys;
	string								m_entry_prompt;
    string                              m_dialog_name;
	string								m_affirm_resp;
	string								m_neg_resp;

	// Evaluation

    vector<string> Tokenize(string Utterance);

	DialogNode::Method ProcessDialogKeys(
        vector<string>& dialogKeys,
		StagingData* StagingInstance,
        string& response);

    DialogNode::Method ProcessQuery(
        vector<string> pNodeValues,
        StagingData* StagingInstance,
        string& response);

	Dialog::RESPONSE FindPotentialSolutions(
		vector<string> answers,
		vector<long long>& branches,
		vector<long long>& temp_trees,
		map<long long, vector<vector<DialogNode*>>> &Solutions);

	string BuildResponse(vector<string> answers, RESPONSE option);

    bool AnswerValidSolutions(map<long long, vector<vector<DialogNode*>>> &Solutions,
        vector<long long> temp_branches,
        StagingData* StagingInstance,
        size_t num_answers);

    vector<DialogNode*> FindQueriedResponses(vector<long long> QueriedBranches, vector<DialogNode*> ChildNodes);

    bool FindImpliedResponsesFlag;
    void FindImpliedResponses(StagingData* StagingInstance);

	bool CompareBranches(DialogNode* node, vector<long long>& branch);

	bool CompareTrees(DialogNode* node, vector<long long>&);
};

