#include "DialogInclude.h"

string ERRORLEVEL2STRING(ERROR_LEVEL level) {
    switch (level) {
    case (NORMAL) :
        return "   NORMAL";
        break;
    case (VERBOSE) :
        return "++ NORMAL";
        break;
    case (USER_SEVERE) :
        return "   SEVERE USER ERROR";
        break;
    case (USER_CRITICAL) :
        return "   CRITICAL USER ERROR";
        break;
    case (ABORT) :
        return "!! PROGRAMMER ERROR";
        break;
    default:
        return "-- UNDEFINED ERROR";
        break;
    }
}

void REPORT_ERROR(string message, ERROR_LEVEL level) {
    fprintf(stderr, "%s ===> %s\n", ERRORLEVEL2STRING(level).c_str(), message.c_str());
}

bool IS_NOT_ERROR(bool expression, string message, ERROR_LEVEL level) {
    if (!expression) {
        fprintf(stderr, "%s ===> %s\n", ERRORLEVEL2STRING(level).c_str(), message.c_str());
    }
    return expression;
}

bool IS_ERROR(bool expression, string message, ERROR_LEVEL level) {
    if (!expression) {
        fprintf(stderr, "%s ===> %s\n", ERRORLEVEL2STRING(level).c_str(), message.c_str());
    }
    return !expression;
}

char FirstCharacter(string line) {
    if (line.length() <= 0) {
        return ' ';
    }
    std::size_t found = line.find_first_not_of("\t\n ");
    if (found!=std::string::npos)
    {
      return line[found];
    }
    return line[0];
}

void ReadSpecificationFile(string & XML_File, string & XML_Text, vector<string> & Expression_Text, bool IgnoreCase) {
    // Open File
    ifstream is;
    is.open(XML_File.c_str(), ios::in);
    // Read in Text
    string buffer;
    while (is.is_open() && !(is.eof())) {
        getline(is, buffer);
        if (buffer.length() <= 0) {
            continue; // Blank Line
        }
        char ch = FirstCharacter(buffer);
        if (ch == '#') {
            continue; // Line Comment
        }
        else if (ch == '<') {
            XML_Text.append(buffer);
        }
        else if (ch != ' ') {
            if (ch == '|' && Expression_Text.size() > 0) {
                Expression_Text.at(Expression_Text.size()-1).append(buffer);
            }
            else {
                Expression_Text.push_back(buffer);
            }
        }
    }

    // Close File
    is.close();

    if (IgnoreCase) {
        // Convert all data to lowercase!
        std::transform(XML_Text.begin(), XML_Text.end(), XML_Text.begin(), ::tolower);
        for (int i = 0; i< Expression_Text.size(); ++i) {
                    std::transform(Expression_Text.at(0).begin(),
                                   Expression_Text.at(0).end(),
                                   Expression_Text.at(0).begin(), ::tolower);
        }
    }
}

std::string trim(const std::string& str,
                 const std::string& whitespace)
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

std::string reduce(const std::string& str,
                   const std::string& fill,
                   const std::string& whitespace)
{
    // trim first
    auto result = trim(str, whitespace);

    // replace sub ranges
    auto beginSpace = result.find_first_of(whitespace);
    while (beginSpace != std::string::npos)
    {
        const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
        const auto range = endSpace - beginSpace;

        result.replace(beginSpace, range, fill);

        const auto newStart = beginSpace + fill.length();
        beginSpace = result.find_first_of(whitespace, newStart);
    }

    return result;
}

void split(const std::string &s, string rx, std::vector<std::string> &elems) {
    regex rex(rx);
    std::copy( std::sregex_token_iterator(s.begin(), s.end(), rex, -1),
                   std::sregex_token_iterator(),
                   std::back_inserter(elems));
}
