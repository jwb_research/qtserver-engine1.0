/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the project nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "StagingData.h"


StagingData::StagingData(DialogNode* nStartNode) :
mCurrentNode(nStartNode)
{

}

StagingData::StagingData(StagingData* nStagingData) {
	this->mAnsweredNodes = nStagingData->mAnsweredNodes;
	this->mAnsweredValues = nStagingData->mAnsweredValues;
	this->mCurrentNode = nStagingData->mCurrentNode;
	this->mBranchVector = nStagingData->mBranchVector;
	this->mTreeVector = nStagingData->mTreeVector;
}

StagingData::~StagingData()
{
	this->mCurrentNode = NULL;
	this->mAnsweredNodes.clear();
}

int StagingData::GetQuestionsRemaining() {
    return this->mCurrentNode->GetNodesRemaining();
}

bool StagingData::IsFinished() {
	return mCurrentNode->IsLeafNode();
}

string StagingData::GetSolicitation() {
    return this->mCurrentNode->GetSolicitation();
}

void StagingData::SetAnswer(DialogNode* parent, DialogNode* child) {
	map<DialogNode*, DialogNode*>::iterator it;
	if ((it = this->mAnsweredNodes.find(parent)) != this->mAnsweredNodes.end()) {
		// Check for duplicate answer
		if (it->second != child) {
			abort(); // Already answered!!! (should never happen)
		}
	}
	else {
		this->mAnsweredNodes[parent] = child;
		this->mAnsweredValues.push_back(child->GetValue());
	}
}

bool StagingData::IsAnswered(string nAnswerToCheckFor) {
    return (find(this->mAnsweredValues.begin(),
                 this->mAnsweredValues.end(),
                 nAnswerToCheckFor) != this->mAnsweredValues.end());
}

vector<string> StagingData::GetAnswers() {
    return this->mAnsweredValues;
}

int StagingData::GetNumberOfAnswers() {
    return this->mAnsweredNodes.size();
}

void StagingData::ResetCurrentNode() {
	map<DialogNode*, DialogNode*>::iterator it;
	while ((it = this->mAnsweredNodes.find(this->mCurrentNode)) != this->mAnsweredNodes.end()) {
		std::cerr << "Console: Current Point Changed From " << this->mCurrentNode->GetValue() << " To ";
		this->mCurrentNode = it->second;
		std::cerr << this->mCurrentNode->GetValue() << endl;
	}
}

void StagingData::RestartCurrentNode(DialogNode* nStartNode) {
	this->mCurrentNode = nStartNode;
	this->ResetCurrentNode();
}

string StagingData::GetChoicesListAsString(vector<DialogNode*>* LimitedChoices) {
    vector<DialogNode*> NodeChoices  = this->mCurrentNode->Children(this->mBranchVector);
    if (LimitedChoices) {
        NodeChoices = *LimitedChoices;
    }

    if (NodeChoices.size() == 1) {
        // TODO: can answer this -- Maybe already taking care of implied responses?
    }
    stringstream ChoicesStr;
    ChoicesStr << left << setw(40) << "Choice: " << "Details:" << endl;
    ChoicesStr << "----------------------------------------------------------------------\n";
    for (DialogNode* node : NodeChoices) {
        stringstream output;
        output << "\"" << node->GetValue() << "\"";
        ChoicesStr << left << setw(40) << output.str() << node->GetDetails() << endl;
    }
    return ChoicesStr.str();
}
