/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once

#include "DialogInclude.h"
#include "Dialog.h"
#include "Staging/StagingData.h"

#define MAX_UNDO_SIZE 10
#define MAX_REDO_SIZE 10

class DialogStager
{
public:
	DialogStager(Dialog* nDialog);
	~DialogStager();

    //                              Used for testing
    int                             ExecuteDialog(bool showChoices = true);

    //                              Functions that return a response
    string                          GetSolicitation(bool showChoices = false);
    string                          GetStartPrompt();
    string                          Evaluate(string Utterance);
    string                          Redo();
    string                          Undo();
    string                          Restart();

    int                             GetQuestionsRemaining();
    int                             GetNumberOfAnswers();

private:

    vector<string>                  ParseUtterance(string Utterance); // Can be made more complex later

    void                            PushBackStagingData();
    void                            PopBackStagingData();
    void                            ClearRedoHistory();

    StagingData*                    mStagingData;
    vector<StagingData*>            mStagingDataUndoStack;
    vector<StagingData*>            mStagingDataRedoStack;
    Dialog*                         mDialog;
};

