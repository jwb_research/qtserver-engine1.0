/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "Dialog.h"

Dialog::Dialog(string dialog_name, string start_prompt, bool CreateDictionary) :
    m_entry_prompt(start_prompt),
    m_dialog_name(dialog_name)
{
    this->FindImpliedResponsesFlag = true;
    // TODO: Load config file

    this->m_affirm_resp = "Ok, ";
    this->m_neg_resp = "Sorry, ";

    if (CreateDictionary) {
        this->mDictionary = new NLPUDictionary();
    }
}

// TODO: Destructor
Dialog::~Dialog()
{
	// TODO:
}

DialogNode* Dialog::AddRootToDialog(string eval_type, long long treeID) {
	DialogNode::Method eval = DialogNode::String2Method(eval_type);
	this->m_EVAL_LOOKUP[treeID] = eval; // Add to table

	// Add Root to mStartNodes
    stringstream name;
    name << "__ROOT_" << this->mStartNodes.size();
    this->mStartNodes.push_back(new DialogNode("", // TODO: replace empty string with context
        name.str(), " (No Details) ", eval, treeID, 0, false, true, 0));

	// Add to tree map
	this->m_SUBDIALOG_LOOKUP[treeID] = this->mStartNodes.at(this->mStartNodes.size() - 1);

    // Parent of root node is the root node...Ha!
     m_PARENT_TREE_LOOKUP[treeID] = treeID;

	// Return newest Root
	return this->mStartNodes.at(this->mStartNodes.size() - 1);
}

DialogNode* Dialog::AddNodeToDialog(vector<DialogNode*> nParentNodes,
        string value,
        string desc_statement,
        string prompt,
        string nDetails,
        long long tree_id,
        string eval_type,
        int endsNDialogs)
{
    // Must be at least one parent node!

	// Variables
	bool is_sub_dialog = false;
    long long nTreePosition = 1 + nParentNodes.at(0)->GetTreePosition();

	// Get eval method
	int Ends_N_Sub_dialogs = 0;
    if (endsNDialogs > 0) {
        eval_type = "";
        Ends_N_Sub_dialogs = endsNDialogs;
	}
	DialogNode::Method eval;
	map<long long, DialogNode::Method>::iterator it;
	it = this->m_EVAL_LOOKUP.find(tree_id);
	// New Eval Type specified
	if (eval_type.size() > 0) {
        eval = DialogNode::String2Method(eval_type);
        if (it != this->m_EVAL_LOOKUP.end() && it->second != eval) {
            cout << "THIS SHOULD NEVER HAPPEN!!!!";
			abort();
		}
        else {
            this->m_EVAL_LOOKUP[tree_id] = eval; // Add to table
        }
		is_sub_dialog = true;
		nTreePosition = 1;
	}
	else if (it != this->m_EVAL_LOOKUP.end()) {
		eval = it->second;
	}
	else {
        cout << "THIS SHOULD NEVER HAPPEN!!!!";
		abort();
	}

    // Can happen now - no it cant 02-11-17
//    if (nParentNodes.at(0)->SetSolicitation(prompt).compare(prompt) != 0) {
//        cout << "Somehow attached responses with different prompts! THIS SHOULD NEVER HAPPEN!!!!";
//    }

    DialogNode* new_child = new DialogNode (desc_statement, value, nDetails, eval, tree_id, nTreePosition, false,false, Ends_N_Sub_dialogs); // , is_sub_dialog);
    this->AddNodeToDialogTable(new_child);

    // If sub dialog, add node to table
    if (is_sub_dialog) {
        if (nParentNodes.at(0)->mEndsNSubTrees == 0) {
            this->m_PARENT_TREE_LOOKUP[tree_id] = nParentNodes.at(0)->mTreeID;// contains the tree with the right eval mechanic
        }
        else {
            long long TempID = nParentNodes.at(0)->mTreeID;
            for (int i = nParentNodes.at(0)->mEndsNSubTrees; i > 0; --i) {
                TempID = this->m_PARENT_TREE_LOOKUP[TempID];
            }
            this->m_PARENT_TREE_LOOKUP[tree_id] = TempID;// contains the tree with the right eval mechanic
        }
        this->m_SUBDIALOG_LOOKUP[tree_id] = nParentNodes.at(0); // contains the prompt only, not the eval mechanic
        cout << "Tree " << tree_id << " = " << value << " " << prompt << endl;
    }

    // For each parent node
    for (DialogNode* nParentNode: nParentNodes) {
        nParentNode->LinkToNode(new_child);
        this->RemoveIfEndNode(nParentNode);
        // Set parent prompt will only do so if parent has no prompt
        nParentNode->SetSolicitation(prompt);
    }
	return new_child;
}

// May not need afterall
vector<DialogNode*>	Dialog::GetStartNodes() {
	return this->mStartNodes;
}

bool Dialog::AddNodeToDialogTable(DialogNode* node) {
	std::map<string, Nodes*>::iterator it, it_end;
	it = this->m_ALL_NODES_IN_DIALOG.find(node->GetValue());
	if (it != this->m_ALL_NODES_IN_DIALOG.end()) {
		// Node to add already exists
		it->second->push_back(node);
	}
	else {
		Nodes* node_vec = new vector<DialogNode*>();
		node_vec->push_back(node);
		this->m_ALL_NODES_IN_DIALOG[node->GetValue()] = node_vec;
		this->m_DESC_STATEMENTS[node->GetValue()] = node->GetContextGrammar();
	}
	this->m_END_POINTS[node] = node;
	return true;
}

bool Dialog::RemoveIfEndNode(DialogNode* node) {
	if (node->IsLeafNode()) {
		node->mLeafNode = false;
		this->m_END_POINTS.erase(node);
	}
	return true;
}

bool Dialog::CompleteDialog() {
	// For all end points:
	//     Label branches by traversing the tree
    int branchID = 0;
    for (DialogNode* root : this->mStartNodes) {
        this->RootLabelAllBranches(root, branchID);
	}
    std::cerr << "Console: Number of Endpoints = " << branchID << endl;

	//////////////////////////////////////////
	// Retrieve all keys
	transform(this->m_ALL_NODES_IN_DIALOG.begin(), this->m_ALL_NODES_IN_DIALOG.end(), back_inserter(m_all_keys), RetrieveKey());
	// Dump all keys
	std::cerr << "Console: Keys:" << endl;
    for (vector<string>::iterator it = m_all_keys.begin(); it != m_all_keys.end(); ++it) {
        std::cerr << *it << "\n";
    }
    //copy(m_all_keys.begin(), m_all_keys.end(), ostream_iterator<string>(std::cerr, "\n"));
    //////////////////////////////////////////

    // Generate Dictionary Using NLPU if Dictionary != NULL
    if (mDictionary) {
        mDictionary->GenerateDictionary(m_all_keys);
        vector<string> Terms = mDictionary->GetAllTerms();
        std::cerr << "ALL TERMS: \n";
        copy(Terms.begin(), Terms.end(), ostream_iterator<string>(std::cerr, "\n"));
    }

	return true;
}

 vector<long long> Dialog::RootLabelAllBranches(DialogNode* node, int& branchID) {
    vector<long long> New_Branches;
    if (node->IsLeafNode()) {
        ++branchID;
        node->mBranchVector.push_back(branchID);
        New_Branches.push_back(branchID);
        node->mEndsNSubTrees = 1; // All leaf nodes end dialogs!
    }
    else {
        map<string, DialogNode*>::iterator it;
        for (it = node->mChildMap->begin();
             it != node->mChildMap->end(); it++) {
             vector<long long> Temp_Branches = this->RootLabelAllBranches(it->second, branchID);
             New_Branches.reserve( New_Branches.size() + Temp_Branches.size() ); // preallocate memory
             New_Branches.insert( New_Branches.end(), Temp_Branches.begin(), Temp_Branches.end() );
         }
        node->mBranchVector.reserve( node->mBranchVector.size() + New_Branches.size() ); // preallocate memory
        node->mBranchVector.insert( node->mBranchVector.end(), New_Branches.begin(), New_Branches.end() );
    }
    return New_Branches;
}

size_t Dialog::LabelAllBranches(DialogNode* node, int branchID, size_t numCalls) {
	if (node->mBranchVector.size() == 0 || (node->mBranchVector.at(node->mBranchVector.size() - 1) != branchID)) {
        // Add branch to local branch vector
        node->mBranchVector.push_back(branchID);
        // Add branch to global branche vector
        map<string, vector<long long> >::iterator it_vec;
        it_vec = this->m_NODE_NAME_2_BRANCHES.find(node->GetValue());
        if (it_vec != this->m_NODE_NAME_2_BRANCHES.end()) {
                it_vec->second.push_back(branchID);
        }
        else {
            this->m_NODE_NAME_2_BRANCHES[node->GetValue()] = vector<long long>(1, branchID);
        }
        // Add branch to all parents
		std::map<string, DialogNode*>::iterator it;
		for (it = node->mParentMap->begin();
			it != node->mParentMap->end(); it++) {
			numCalls += this->LabelAllBranches(it->second, branchID, numCalls);
		}
		return ++numCalls; // incriment for every call
	}
    else {
        // Update 8 January 2017
        // This happens with the new dialog generation
        // With the simplified exploded dialog tree

        // Can this happen?? no? Maybe for @fields which don't exist?
        //cout << "Confirm CASE: See LabelAllBranches()\n";
        // TODO: If else statment can probably be removed.
    }
	return numCalls;
}

std::vector<string>::iterator findPartial(std::vector<string>::iterator first, 
	std::vector<string>::iterator last, const string val)
{
	while (first != last) {
		// Check if substring and if substring starts at index 0;
		if (first->find(val) != std::string::npos && first->find(val) == 0) {
			return first;
		}
		++first;
	}
	return last;
}

int Dialog::searchKeysFor(string key) {
	// using std::find with vector and iterator:
	std::vector<string>::iterator it;

	it = findPartial(this->m_all_keys.begin(), this->m_all_keys.end(), key);
	if (it != this->m_all_keys.end()) {
		
		// note: multiple matches may exist so it is neccessary to only add one character at a time to key
		// ----- but shorter matches will always be found first
		return it->size() - key.size(); // number of characters that still need to be matched
	}
	return -1; // if not found at all
}

string Dialog::GetStartPrompt() {
	return this->m_entry_prompt;
}

string Dialog::getAffirmResp() {
	return this->m_affirm_resp;
}

string Dialog::getNegResp() {
	return this->m_neg_resp;
}

///////////////////////////////////////////////////////////////////////
//                                                                   //
//                           Evaluation:                             //
//                                                                   //
///////////////////////////////////////////////////////////////////////

 vector<string> Dialog::Tokenize(string Utterance) {
     if (this->mDictionary) {
         return mDictionary->Tokenize(Utterance);
     }

     // Else TODO: use old method using string compare?
     return  vector<string>();
 }

DialogNode::Method Dialog::ProcessQuery(vector<string> answers,
    StagingData* StagingInstance,
    string&response)
{
    //  Check for valid tree and branch
    // -----------------------------------
    // Branches and trees cannot be set until all answers are checked
    vector<long long> temp_branches;
    vector<long long> temp_trees;

    // Store possible valid nodes
    map<long long, vector<vector<DialogNode*>>> potential_solutions;

    RESPONSE resp;
    if ((resp = FindPotentialSolutions(answers, temp_branches, temp_trees, potential_solutions)) != RESPONSE::TRUE || potential_solutions.size() == 0)  {
        response = "Sorry, query did not produce any results."; // No potential Solutions
        return DialogNode::INVALID;
    }
    // Only care about the branches not the solutions.
    // Want to compare answers to the same question on temp branches to the answers for current question.
    // Overlap is what we want

    // First get all responses from temp branches
    vector<DialogNode*> queriedNodes =
            this->FindQueriedResponses(temp_branches,
                                       StagingInstance->mCurrentNode->Children(StagingInstance->mBranchVector));

    stringstream QueryStrm;
    if (queriedNodes.size() > 0) {
        QueryStrm << "Here is what I found matching your query:\n" <<
                     StagingInstance->GetChoicesListAsString(&queriedNodes);
        response = QueryStrm.str();
    }

    // Exit
    return StagingInstance->mCurrentNode->GetEvaluationMethod();
}

vector<DialogNode*> Dialog::FindQueriedResponses(vector<long long> QueriedBranches, vector<DialogNode*> ChildNodes) {
    vector<DialogNode*> queriedChildren;
    for (DialogNode* node : ChildNodes) {
        vector<long long> test_branches = this->m_NODE_NAME_2_BRANCHES.at(node->GetValue());
        vector<long long> newBranch(test_branches.size());
        vector<long long>::iterator it =
            set_intersection(test_branches.begin(), test_branches.end(),
            QueriedBranches.begin(), QueriedBranches.end(), newBranch.begin());
        newBranch.resize(it - newBranch.begin());
        if (newBranch.size()) {
            queriedChildren.push_back(node);
        }
    }
    return queriedChildren;
}

/*
/ Function: ProcessUtterance
/
/
*/
DialogNode::Method Dialog::ProcessDialogKeys(vector<string> &dialogKeys,
    StagingData* StagingInstance,
    string& response)
{
	//  Check for valid tree and branch
	// -----------------------------------
	// Branches and trees cannot be set until all answers are checked
	vector<long long> temp_branches = StagingInstance->mBranchVector;
	vector<long long> temp_trees = StagingInstance->mTreeVector;

	// Store possible valid nodes
	map<long long, vector<vector<DialogNode*>>> potential_solutions;

	RESPONSE resp;
    if ((resp = FindPotentialSolutions(dialogKeys, temp_branches, temp_trees, potential_solutions)) != RESPONSE::TRUE || potential_solutions.size() == 0)  {
        response = this->BuildResponse(dialogKeys, resp); // No potential Solutions

		// TODO: 
		// Maybe Question was already answered with something else?
		// Maybe user wants to change answer?
		// Can do lots of things here...

		return DialogNode::INVALID;
	}

	// Evaluate Potential Solutions
	// -----------------------------------
    if (!AnswerValidSolutions(potential_solutions, temp_branches, StagingInstance, dialogKeys.size())) {
        response = this->BuildResponse(dialogKeys, RESPONSE::Sub_Parent_Dialog_MisMatch); // No valid Solutions

		// TODO:
		// Do anything here?

		return DialogNode::INVALID;
	}

	// Valid Solutions Evaluated, Update StagingInstance
	// -----------------------------------

	// if answered current question, choose next unanswered question on current branch
	StagingInstance->ResetCurrentNode();
	if (StagingInstance->mCurrentNode->mEndsNSubTrees > 0) {
        // May have exited subdialog so do full restart (SPE' only)
        StagingInstance->RestartCurrentNode(this->mStartNodes.at(0)); // TODO: this may only work if only one dialog is loaded
	}

    // Update Branches
    StagingInstance->mBranchVector = temp_branches;

    // Find Implied Responses
    if (this->FindImpliedResponsesFlag) {
        this->FindImpliedResponses(StagingInstance);
    }

	// Build Response
    response = this->BuildResponse(dialogKeys, TRUE);
    std::cerr << "Console: Answered " << dialogKeys.size() << " Question(s)" << endl;
	std::cerr << "Console: There is/are " << temp_branches.size() << " solutions(s) remaining." << endl;

	// Exit
	return StagingInstance->mCurrentNode->GetEvaluationMethod();
}


void Dialog::FindImpliedResponses(StagingData* StagingInstance) {

    // Check for implied response!
    vector<DialogNode*> PossibleResponses = StagingInstance->mCurrentNode->Children(StagingInstance->mBranchVector);
    if (PossibleResponses.size() == 1) {
        // Answer Implied Response
        DialogNode* implied_response = PossibleResponses.at(0);
        cerr << "Console: Implied Response: " << implied_response->GetValue() << endl;
        // setparents as answered but check if the parent is already answered!
        map<string, DialogNode*>::iterator it_p;
        for (it_p = implied_response->mParentMap->begin(); // typically only one parent 4/25/16 - JWB
            it_p != implied_response->mParentMap->end(); it_p++)
        {
            StagingInstance->SetAnswer(it_p->second, implied_response);
        }

        // if answered current question, choose next unanswered question on current branch
        StagingInstance->ResetCurrentNode();
        if (StagingInstance->mCurrentNode->mEndsNSubTrees > 0) {
            // May have exited subdialog so do full restart (SPE' only)
            StagingInstance->RestartCurrentNode(this->mStartNodes.at(0));
        }

        this->FindImpliedResponses(StagingInstance);
    }
}

/*
/ Function: BuildResponse
/
/
*/
string Dialog::BuildResponse(
	vector<string> answers,
	RESPONSE option) {
	// create response
	stringstream ss; // Response string stream
	switch (option) {
	case (TRUE) :
		ss << this->getAffirmResp();
		break;
	case (FALSE) :
		ss << this->getNegResp();
		break;
	case (Sub_Parent_Dialog_MisMatch) :
		return "Sorry, I could not process that quite right, please reply only to the prompt:";
		break;
	}

	for (unsigned int i = 0; i < answers.size(); i++) {
		string answer = answers.at(i);
		if (i != 0) {
			ss << " and ";
		}
		ss << m_DESC_STATEMENTS.find(answer)->second << answer;
	}
	ss << ".";
	return ss.str();
}

/*
/ Function: AnswerValiContextGrammarolutions
/
/
*/
bool Dialog::AnswerValidSolutions(
    map<long long, vector<vector<DialogNode*> > > &Solutions,
	vector<long long> temp_branches,
	StagingData* StagingInstance,
    size_t num_answers) {

	bool atLeastOne = false;

    map<long long, vector<vector<DialogNode*> > >::iterator solution_it;
	for (solution_it = Solutions.begin(); solution_it != Solutions.end(); solution_it++) {

		bool valid = true;

		if (num_answers != solution_it->second.size()) {
				cerr << "Console: Potential Child Solution INCOMPLETE: 100" << endl;
				valid = false;
				continue;
		}

        // Determine the parent node (may not be current node if current node ends a dialog)
//		DialogNode *parent_node = StagingInstance->mCurrentNode;
// recheck for right parent

//        // ALL THIS SECTION CAN BE REMOVED PROBABLY
//        cout << "Parent is " << parent_node->GetValue() << endl << "tree id: " << parent_node->mTreeID << endl;
//        // Get proper eval mechanic
//		if (parent_node->mEndsNSubTrees > 0) {
//            cout << "ends " << parent_node->mEndsNSubTrees << " dialogs..." << endl;
//            for (long long tree : parent_node->mTreeVector) {
//                cout << tree << " ";
//            }
//            cout << endl;
//            // tree vector should store the parent trees not the child trees? need new mechanism?
//           parent_node = this->m_SUBDIALOG_LOOKUP[parent_node->mTreeVector.at(parent_node->mTreeVector.size() - 1 - (size_t)parent_node->mEndsNSubTrees)];
//		}
        // //////////////////////////////////////////

		// Check for subdialogue validity against parent dialog
        if (StagingInstance->mCurrentNode->mTreeID != solution_it->first) {
			// IN SUBDIALOG -------------
            // Parent eval mechanic
//            parent_node = this->m_SUBDIALOG_LOOKUP[solution_it->first]; // recheck for right parent
            // Get proper eval mechanic
            long long treeID = m_PARENT_TREE_LOOKUP[solution_it->first];
            DialogNode *parent_node = this->m_SUBDIALOG_LOOKUP[solution_it->first];
            while (parent_node->mEndsNSubTrees > 0) {
                long long tempTree;
                for (int i = parent_node->mEndsNSubTrees; i > 0; --i) {
                    cerr << "Console: Parent tree changed from " << treeID;
                    tempTree = treeID;
                    treeID = m_PARENT_TREE_LOOKUP[treeID];
                    cerr << " to " << treeID << endl;
                }
                parent_node = this->m_SUBDIALOG_LOOKUP[tempTree];
            }
            // Determine the correct eval method
            cout << treeID << " Parent: " << parent_node->GetValue() << endl;
            DialogNode::Method eval = this->m_EVAL_LOOKUP[treeID];
            cout << treeID << endl;
            switch (eval) { // Need to get the dialog method not the current method!
			case (DialogNode::Currying) :
                // Check if need to change Roots
                if (!atLeastOne && (StagingInstance->mCurrentNode != parent_node) && (StagingInstance->mCurrentNode->GetTreeID() == 1)
                        && StagingInstance->mCurrentNode->IsRoot() && parent_node->IsRoot()) {
                    cout << "Console: SubDialog Current Node set from " << StagingInstance->mCurrentNode->GetValue();
                    StagingInstance->mCurrentNode = parent_node;
                    cout << " to " << StagingInstance->mCurrentNode->GetValue() << endl;

                }
                // Current point MUST be parent of the tree
                if (find(StagingInstance->mCurrentNode->mTreeVector.begin(), StagingInstance->mCurrentNode->mTreeVector.end(), solution_it->first) ==
                        StagingInstance->mCurrentNode->mTreeVector.end()) {
                        cerr << "Console: Potential Child Solution FAILED: 101" << endl;
                        valid = false;
                        continue; // Next Solution
                }
                cerr << "Console: Potential Child Solution SUCCEEDED: 0" << endl;
				break;
//			case (DialogNode::SingleArgument_Partial_Evaluation) :
//				// Current parent dialog is SPE' - Change CurrentPoint to start of sub-dialog

//                // TODO: Need to make sure that we are not already in a sub dialog due to current node not changing!
//                // set current node to the parent node of the current tree... not the parent eval strategy
//                StagingInstance->mCurrentNode = this->m_SUBDIALOG_LOOKUP[solution_it->first];
//                // Set Staging Instance Head to Current Tree
//                StagingInstance->mCurrentTreeHead = this->m_SUBDIALOG_LOOKUP[solution_it->first];
//                StagingInstance->mTempSubTreeVector.push_back(1);
//                subtreeThisTime = true;
//                cout << "Current Node=" << StagingInstance->mCurrentNode->GetValue() << endl;
//				break;
			default:
				// Not a supported Sub Dialog
				cerr << "Console: INVALID PARENT DIALOG: 106" << endl;
				continue;
				break;
			}
		}

		// Outer vector specifies number of unique keywords
        // Inner vector specifies the number of times the keyword exists in a valid solution
		vector<vector<DialogNode*>>::iterator nodes;
		DialogNode::Method method = this->m_EVAL_LOOKUP[solution_it->first];
		vector<long long> TreePositions;
		long long LastPosition = 0;

			// Check for validity of this dialog
			// Continue if invalid or break if valid
			switch (method) {
			case (DialogNode::Currying) : // C
				// Must be one arguement and must be to current question
				if (solution_it->second.size() > 1 || // nodes.size() cannot be more than one
					StagingInstance->mCurrentNode->mChildMap->find(solution_it->second.at(0).at(0)->GetValue()) ==
					StagingInstance->mCurrentNode->mChildMap->end()) 
				{
					cerr << "Console: Potential C Solution FAILED: 103" << endl;
					valid = false;
					continue; // Next Solution
				}
				break;
			case (DialogNode::SingleArgument_Partial_Evaluation) : // SPE'
				// Must be one arguement...
				if (solution_it->second.size() > 1) {
					cerr << "Console: Potential SPE' Solution FAILED: 104" << endl;
					valid = false;
					continue; // Next Solution
				}
				break;
			case (DialogNode::Interpretation) : // I
				// Full path must be specified so number of nodes in solution must be number of nodes in subdialog!
				// There will only be one path
				for (nodes = solution_it->second.begin(); nodes != solution_it->second.end(); nodes++) {
					for (DialogNode* node : *nodes) {
						if (node->GetEndsNSubTrees() > 0) {
							LastPosition = node->GetTreePosition();
						}
						TreePositions.push_back(node->GetTreePosition());
					}
				}
				sort(TreePositions.begin(), TreePositions.end());
				TreePositions.erase(unique(TreePositions.begin(), TreePositions.end()), TreePositions.end());
                if ((LastPosition == 0) || (LastPosition != TreePositions.size()) ||
                    (TreePositions.size() != TreePositions.at(TreePositions.size() - 1))) {
					cerr << "Console: Potential I Solution FAILED: 105" << endl;
					valid = false;
					continue;
				}
				break;
			case (DialogNode::Partial_Evaluation_Application) : // PFAn
				// unbroken path must be specified!
				for (nodes = solution_it->second.begin(); nodes != solution_it->second.end(); nodes++) {
					for (DialogNode* node : *nodes) {
						TreePositions.push_back(node->GetTreePosition());
					}
				}
				sort(TreePositions.begin(), TreePositions.end());
				TreePositions.erase(unique(TreePositions.begin(), TreePositions.end()), TreePositions.end());
				if (TreePositions.size() != (TreePositions.at(TreePositions.size() - 1) - StagingInstance->mCurrentNode->GetTreePosition())) {
					cerr << "Console: Potential PFAn Solution FAILED: 107" << endl;
					valid = false;
					continue;
				}
				break;
			case (DialogNode::Partial_Evaluation) : // PE*
				// Everything goes.
				break;
			default:
				abort();
				break;
			}

		if (!valid) {
			continue; // Next Solution
		}

		for (nodes = solution_it->second.begin(); nodes != solution_it->second.end(); nodes++) {
			// Answer all of the nodes that are still on valid branch
			for (DialogNode* node : *nodes) {
				// Answer Node if still on branch
				vector<long long> branches_copy = temp_branches;
				// garenteed to be on tree by GetOnlyNodesOnBranch()
				if (this->CompareBranches(node, branches_copy)) {
					// setparents as answered but check if the parent is already answered!
					map<string, DialogNode*>::iterator it_p;
                    for (it_p = node->mParentMap->begin(); // typically only one parent 4/25/16 - JWB
						it_p != node->mParentMap->end(); it_p++) 
					{
                        StagingInstance->SetAnswer(it_p->second, node);
					}
					atLeastOne = true;
				}
				else {
                    cerr << "=====> This can happen... Node: " << node->GetValue() << "\n"; // This has been verified to occur 4/25/16 - JWB.
                    //abort();

                    // Ignore this node as it is not on the path...
				}
            } // End for (DialogNode* node : *nodes)

        } // End for (nodes = solution_it->second.begin(); nodes != solution_it->second.end(); nodes++)

		cerr << "Console: Potential Solution SUCCEEDED: 0" << endl;
	}
	return atLeastOne;
}

/*
/ Function: FindPotentialSolutions
/
/	All answers must be on the same tree(s)
/	All answers must be on the same branch(es)
/
/	Creates a map of vectors with keys cooresponding to trees in the dialog
/	Each Vector stores a vector for each answer in the solution
/	If the vector does not contain a vector for eah of the answers, the solution is incomplete
/	There may be multiple copies of an answer per tree (thus the us of vectors of vectors)
/
*/
Dialog::RESPONSE Dialog::FindPotentialSolutions(
	vector<string> answers,
	vector<long long>& branches,
	vector<long long>& temp_trees,
	map<long long, vector<vector<DialogNode*>>> &Solutions) 
{

	size_t answer_num = 0;

	RESPONSE AtLeastOne;

	for (string answer : answers) {
		// Get Nodes for Answer
		map<string, Nodes*>::iterator it;
		it = this->m_ALL_NODES_IN_DIALOG.find(answer);
		if (it == this->m_ALL_NODES_IN_DIALOG.end()) {
            abort(); // should never happen since answer must be a node
		}

		AtLeastOne = FALSE;

		vector<long long> branches_copy, temp_branches;
		vector<long long> trees_copy, new_trees;

		// For each node, AND the branches (find intersection)
		for (DialogNode* node : *it->second) {
			branches_copy = branches;
			trees_copy = temp_trees;
			// Intersection of branches_copy and node->branches
			if (this->CompareTrees(node, trees_copy) && this->CompareBranches(node, branches_copy)) {
				AtLeastOne = TRUE;
				// Union Branches
				vector<long long>::iterator end_B;
				vector<long long> temp_B(temp_branches.size() + branches_copy.size());
				end_B = set_union(temp_branches.begin(), temp_branches.end(),
					branches_copy.begin(), branches_copy.end(), temp_B.begin());
				temp_B.resize(end_B - temp_B.begin());
				temp_branches = temp_B;
				// Union Trees
				vector<long long>::iterator end_T;
				vector<long long> temp_T(new_trees.size() + trees_copy.size());
				end_T = set_union(new_trees.begin(), new_trees.end(),
					trees_copy.begin(), trees_copy.end(), temp_T.begin());
				temp_T.resize(end_T - temp_T.begin());
				new_trees = temp_T;

				// Store ok node for now:
				if (Solutions[node->mTreeID].size() - 1 != answer_num) {
					// Create new solution vector for this tree
					Solutions[node->mTreeID].push_back(vector<DialogNode*>());
				}
				// Append to solution vector for this tree
				Solutions[node->mTreeID].at(answer_num).push_back(node);
			}
			else if (AtLeastOne == FALSE){
				branches_copy = branches;
				if (this->CompareBranches(node, branches_copy)) {
					// There is a valid branch but it invalidates the dialog structure....
					AtLeastOne = Sub_Parent_Dialog_MisMatch;
				}
			}
		}
		
		// No matches this round
		if (AtLeastOne != TRUE) {
			cerr << "Console: " << temp_trees.size() << " Potential Solutions" << endl;
			return AtLeastOne;
			break;
		}

		branches = temp_branches;
		temp_trees = new_trees;

		answer_num++;
	}

	cerr << "Console: " << temp_trees.size() << " Potential Solutions" << endl;
	return AtLeastOne;
}

/*
/ Function: CompareTrees
/
/
*/
bool Dialog::CompareTrees(
	DialogNode* node, 
	vector<long long>& currentTree) 
{
	if (currentTree.size() == 0) {
		currentTree.push_back(node->mTreeID);
		return true;
	}
	else { // new branch is intersection
		vector<long long>::iterator it = find(currentTree.begin(), currentTree.end(), node->mTreeID);
		if (it != currentTree.end()) {
			return true;
		}
	}
	return false;
}

/*
/ Function: CompareBranches
/
/
*/
bool Dialog::CompareBranches(
	DialogNode* node, 
	vector<long long>& curentbranch) {
	if (curentbranch.size() == 0) {
		curentbranch = node->mBranchVector;
		return true;
	}
	else { // new branch is intersection
		vector<long long> newBranch(curentbranch.size());
		vector<long long>::iterator it = 
			set_intersection(node->mBranchVector.begin(), node->mBranchVector.end(), 
			curentbranch.begin(), curentbranch.end(), newBranch.begin());
		newBranch.resize(it - newBranch.begin());
		switch (newBranch.size()) {
		case (0) :
			return false;
			break;
		case(1) :
			// Full solution found!!!!
			// TODO: finish dialogue?
			curentbranch = newBranch;
			return true;
			break;
		default:
			curentbranch = newBranch;
			return true;
			break;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
