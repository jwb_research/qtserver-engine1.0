/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the project nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#pragma once

#include <stdlib.h>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <iomanip>
#include <algorithm>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <iterator>
#include <regex>

#include "rapidxml-1.13/rapidxml.hpp"

using namespace rapidxml;
using namespace std;

// Way to retrieve keys from map
struct RetrieveKey
{
	template <typename T>
	typename T::first_type operator()(T keyValuePair) const
	{
		return keyValuePair.first;
	}
};

enum ERROR_LEVEL {
    NORMAL,			// simple update
    VERBOSE,       // for debugging
    USER_SEVERE,	// Causes a problem that prevents program from running
    USER_CRITICAL,		// Causes a problem that may be recovered from but will likely cause more problems
    ABORT			// Will cause a crash
};

string ERRORLEVEL2STRING(ERROR_LEVEL level);

void REPORT_ERROR(string message = "Undefined Error!", ERROR_LEVEL level = NORMAL);

bool IS_NOT_ERROR(bool expression, string message = "Undefined Error!", ERROR_LEVEL level = NORMAL);

bool IS_ERROR(bool expression, string message = "Undefined Error!", ERROR_LEVEL level = NORMAL);

char FirstCharacter(string line);

void ReadSpecificationFile(string & XML_File, string & XML_Text, vector<string> & Expression_Text, bool IgnoreCase);


std::string trim(const std::string& str,
                 const std::string& whitespace = " \t");

std::string reduce(const std::string& str,
                   const std::string& fill = " ",
                   const std::string& whitespace = " \t");

void split(const std::string &s, string rx, std::vector<std::string> &elems);

// Ideas:
/*

Allow some Qs to have multiple answers such as "morning or afternoon"


















*/
