#ifndef PSEUDODIALOG_H
#define PSEUDODIALOG_H

#include "PseudoPrompt.h"

class PseudoDialog
{
public:
    PseudoDialog(string name, string entry_prompt);

    map<string, PseudoPrompt*> PromptMap;

    string GetName() { return name; }
    string GetPrompt() { return entry_prompt; }

    bool AddPrompt(string Value, PseudoPrompt * prompt);

private:

    string name;
    string entry_prompt;
};

#endif // PSEUDODIALOG_H
