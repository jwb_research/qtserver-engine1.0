#include "PseudoDialog.h"

PseudoDialog::PseudoDialog(string name, string entry_prompt) :
    name(name),
    entry_prompt(entry_prompt)
{

}

bool PseudoDialog::AddPrompt(string Value, PseudoPrompt * prompt) {
    if (this->PromptMap.find(Value) != this->PromptMap.end()) {
        return false;
    }
    this->PromptMap[Value] = prompt;
    return true;
}
