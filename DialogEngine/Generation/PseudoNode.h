#ifndef PSEUDONODE_H
#define PSEUDONODE_H

#include "../DialogInclude.h"

class PseudoNode
{
public:
    PseudoNode(string Value, string whateverElse = "");

    string GetValue() {return mValue;}

private:

    string mValue;

};

#endif // PSEUDONODE_H
