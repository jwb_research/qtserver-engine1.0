#ifndef PSEUDOPROMPT_H
#define PSEUDOPROMPT_H

#include "PseudoNode.h"

class PseudoPrompt
{
public:    
    PseudoPrompt(string Value, string prompt);

    ~PseudoPrompt();

    bool AddResponse(string Value, PseudoNode * response);

    string GetPrompt() {return mPrompt;}
    string GetValue() {return mValue;}

    map<string, PseudoNode*> mResponseMap;

private:

    string mValue;
    string mPrompt;

};

#endif // PSEUDOPROMPT_H
