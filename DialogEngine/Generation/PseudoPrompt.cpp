#include "PseudoPrompt.h"

PseudoPrompt::PseudoPrompt(string Value, string prompt) :
    mValue(Value),
    mPrompt(prompt)
{

}

PseudoPrompt::~PseudoPrompt() {
    // delete all elements in the map
}

bool PseudoPrompt::AddResponse(string Value, PseudoNode *response) {
    if (this->mResponseMap.find(Value) != this->mResponseMap.end()) {
        return false;
    }
    this->mResponseMap[Value] = response;
    return true;
}
