/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the project nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "NLPUThesaurusLookup.h"

NLPUThesaurus::NLPUThesaurus(string idx, string dat )
{
    // open a new thesaurus object
    this->pMT= new MyThes(idx.c_str(),dat.c_str());
}

NLPUThesaurus::~NLPUThesaurus() {
    delete pMT;
}

vector<string> NLPUThesaurus::lookupSynonym(string Word2Check)
{
    vector<string> Synonyms;
    mentry * pmean;
    const char * buf = Word2Check.c_str();
    int len = strlen(buf);
    int count = pMT->Lookup(buf,len,&pmean);
    if (count == 0) {
        return Synonyms;
    }
    // don't change value of pmean
    // or count since needed for CleanUpAfterLookup routine
    mentry* pm = pmean;

    // fprintf(stdout,"   meaning %s\n",pm->defn);
    for (int j=0; j < pm->count; j++) {
       // fprintf(stdout,"       %s\n",pm->psyns[j]);
        Synonyms.push_back(pm->psyns[j]);
    }

    // now clean up all allocated memory
    pMT->CleanUpAfterLookup(&pmean,count);

    return Synonyms;
}
