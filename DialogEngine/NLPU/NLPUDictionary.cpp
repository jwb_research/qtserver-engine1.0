/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the project nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "NLPUDictionary.h"

NLPUDictionary::NLPUDictionary(bool UseThesaurus) :
    UseThesaurus(UseThesaurus),
    mNLPUThesaurus(NULL),
    Dictionary_Keys(NULL)
{
    if (UseThesaurus) {
        string af = "../DialogEngine/MyThes-1.0/th_en_US_new.idx";
        string df = "../DialogEngine/MyThes-1.0/th_en_US_new.dat";

        mNLPUThesaurus = new NLPUThesaurus(af, df);
    }
}

NLPUDictionary::~NLPUDictionary()
{
    delete mNLPUThesaurus;
}

void NLPUDictionary::GenerateDictionary(vector<string> &DialogKeys) {
    this->Dictionary_Keys = new KeyPair[DialogKeys.size()]();

    cout << "There are " << DialogKeys.size() << " Dialog Keys" << endl << endl;

    for (u_long Key_index = 0; Key_index < DialogKeys.size(); ++Key_index) {
        string key = DialogKeys.at(Key_index);

        // Compute Words in Key (if key is multiple words)
        vector<string> words;
        split(key, " ", words); // TODO: worry about punctuation like ','

        // Add key to Dictionary_Keys, number of words in key used to tell if key is completely answered
        KeyPair key_pair; // pair <key name, number of words in key>
        key_pair.first = key;
        key_pair.second = words.size();
        this->Dictionary_Keys[Key_index] = key_pair; // map <ID of key, words in key pair>

        // Add key terms to Dictionary_Indexes
        for (u_long Term_index = 0; Term_index < words.size(); ++Term_index) {
            string str = words.at(Term_index);
            map<string, vector<TermPair>>::iterator dictionary_it; // map <word, IDs of keys containing word>
            dictionary_it = Dictionary_Terms.find(str);
            TermPair newTermPair = TermPair(Key_index, Term_index);
            if (dictionary_it != Dictionary_Terms.end()) { // Term exists for another key
                vector<TermPair> vec = dictionary_it->second;
                vec.push_back(newTermPair);
                Dictionary_Terms[str] = vec;
            }
            else { // New term
                vector<TermPair> vec;
                vec.push_back(newTermPair);
                Dictionary_Terms[str] = vec;
            }

            // Add Synonyms if Thesaurus is enabled
            if (UseThesaurus) {
                // Call LookupThesaurus() on key to suppliment the dictionary
                vector<string> Synonyms = mNLPUThesaurus->lookupSynonym(str);
                for (string Synonym : Synonyms) {
                    dictionary_it = Dictionary_Terms.find(Synonym);
                    if (dictionary_it != Dictionary_Terms.end()) { // Term exists for another key
                        vector<TermPair> vec = dictionary_it->second;
                        vec.push_back(newTermPair);
                        Dictionary_Terms[Synonym] = vec;
                    }
                    else { // New term
                        vector<TermPair> vec;
                        vec.push_back(newTermPair);
                        Dictionary_Terms[Synonym] = vec;
                    }
                }


            }
        }


    }

     cout << "There are " << Dictionary_Terms.size() << " Dictionary Keys" << endl << endl;
}

vector<string> NLPUDictionary::Tokenize(string Utterance) {
    vector<string> DialogKeys;

    // Split utterance into Tokens
    vector<string> Tokens;
    split(Utterance, " ", Tokens); // TODO: worry about punctuation like ','

    // TODO: Handle Sentiment analysis (and general NLP understanding)

    // TODO: Handle the order of words such as "yes cream no sugar" does NOT map to yes and no cream and sugar

    // Lookup Tokens in Dictionary and Create Feature Vector
    map<u_long, vector<uint>> Feature_Vector;
    for (string token : Tokens) {
        map<string, vector<TermPair>>::iterator dictionary_it;
        dictionary_it = Dictionary_Terms.find(token);
        if (dictionary_it != Dictionary_Terms.end()) {
            for (TermPair termpair : dictionary_it->second) {
                map<u_long, vector<uint>>::iterator feat_it = Feature_Vector.find(termpair.first);
                if (feat_it != Feature_Vector.end()) {
                    vector<uint> features = feat_it->second;
                    features.push_back(termpair.second);
                    Feature_Vector[termpair.first] = features;
                }
                else {
                    vector<uint> features;
                    features.push_back(termpair.second);
                    Feature_Vector[termpair.first] = features;
                }
            }
        }
    }

    // Check if Tokens Complete their Respective Keys
    for (map<u_long, vector<uint>>::iterator feat_it = Feature_Vector.begin();
         feat_it != Feature_Vector.end(); ++feat_it) {
        vector<uint> features = feat_it->second;
        map<uint, bool> features_no_duplicates;
        // Remove duplicates
        for (uint feature: features) {
            features_no_duplicates[feature] = true;
        }
        KeyPair testKeyPair = Dictionary_Keys[feat_it->first];
        if (testKeyPair.second == features_no_duplicates.size()) {
            DialogKeys.push_back(testKeyPair.first);
        }
        else {
            cout << "Not enough Tokens for Key: " << testKeyPair.first << endl;

            // TODO: this is where KNN would try to guess
        }
    }

    return DialogKeys;
}

vector<string> NLPUDictionary::GetAllTerms() {
    vector<string> Terms;
    map<string, vector<TermPair>>::iterator dictionary_it;
    for (dictionary_it = Dictionary_Terms.begin(); dictionary_it != Dictionary_Terms.end(); ++dictionary_it) {
        Terms.push_back(dictionary_it->first);
    }
    return Terms;
}
