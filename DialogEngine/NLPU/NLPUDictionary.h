/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the project nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef nlpudictionary
#define nlpudictionary

#include "../DialogInclude.h"

#include "NLPUThesaurusLookup.h"

class NLPUDictionary
{
public:
    NLPUDictionary(bool UseThesaurus = true);
    ~NLPUDictionary();

    void                            GenerateDictionary(vector<string> &DialogKeys);
    vector<string>                  Tokenize(string Utterance);

    vector<string>                  GetAllTerms();

private:

    bool UseThesaurus;

    NLPUThesaurus*                  mNLPUThesaurus;

    // pair <The actual Key, number of words in Key>
    typedef pair<string, uint> KeyPair;

    // pair <Index of KeyPair in Dictionary_Keys, Index of word in KeyPair.TermArray>
    typedef pair<u_long, uint>      TermPair;

    map<string, vector<TermPair>>   Dictionary_Terms;   // maps a word to KeyPair and TermArray Indexes
    KeyPair *                       Dictionary_Keys;    // Array of KeyPair

};

#endif // nlpudictionary
