/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once

#include "DialogInclude.h"

class DialogNode  
{

public:
	enum Method {
		INVALID,
		Interpretation,							// I
		Currying,								// C
		SingleArgument_Partial_Evaluation,		// SPE'
		Partial_Evaluation,						// PE*
		Partial_Evaluation_Application,			// PFAn*
		Interrupt								// Automatially generated SubDialog of Type Interpretation
	};

	enum Relationship {
		PARENT,
		CHILD
	};

	static Method String2Method(string method_string);

private:

	// EVERYTHING PRIVATE

	friend class Dialog;

    typedef long long PathID;

	// CONSTRUCTOR, DESTRUCTOR FUNCTIONS: ---------------------------------------------------------------

    DialogNode(string nContextGrammar,
        string nValue,
        string nDetails,
        Method nEvaluationMethod,
        PathID nTreeID,
        PathID nTreePosition,
        bool nSubTree = false,
        bool nIsRoot = false,
        PathID nExitsNSubDialogs = 0);

    DialogNode(
            DialogNode& node);

	~DialogNode();

	// FUNCTIONS: ---------------------------------------------------------------------------------

	void LinkToNode(
		DialogNode* node, 
		Relationship relationship = CHILD);

    bool CompareBranches(
            vector<PathID> nCurrentBranches);

public:

	// GET/IS FUNCTIONS: --------------------------------------------------------------------------

	string						GetSolicitation();				// Get mSolicitation
	string						GetValue();						// Get mValue
	string						GetContextGrammar();			// Get mContextGrammar
    string                      GetDetails();                   // Get mDtails
	Method						GetEvaluationMethod();			// Get mEvaluationMethod
	bool						IsSubTree();					// Get mSubTree
	bool						IsLeafNode();					// Get mLeafNode
    bool                        IsRoot();                       // Get mIsRoot
    PathID                      GetTreeID();					// Get mTreeID
    PathID                      GetTreePosition();				// Get mTreePosition
    PathID                      GetEndsNSubTrees();				// Get mEndsNSubTrees

    int                         GetNodesRemaining();

    vector<DialogNode*>			Children(vector<PathID> Branch);// Returns children in mChildMap that are on Branch

private:

    string SetSolicitation(string S) {
        if (mSolicitation->length() == 0) {
            mSolicitation = new string(S);
        }
        return *mSolicitation;
    }

	// MEMBER DATA: ---------------------------------------------------------------------------------------

	string*						mSolicitation;					// Node Solicitation (What the avatar says)
	string*						mValue;							// Node Value (What the user says)
	string*						mContextGrammar;				// Node Grammar and Context for avatar responses
    string*                     mDetails;                       // Node Details to be printed with choices
	Method						mEvaluationMethod;				// Node Partial Evaluation Restrictions
	bool						mSubTree;						// Still in use? - maybe to indiate if in a subdialog?
	bool						mLeafNode;						// Is Node a Leaf in the (sub-)dialog
    bool                        mIsRoot;                        // Is node a root node
    PathID                      mTreeID;						// The ID of the (sub-)dialog this node is a part of
    PathID                      mTreePosition;					// Position of this node in the (sub-)dialog (used for I and PFAn)
    PathID                      mEndsNSubTrees;					// Number of sub-dialogs that this node is the end of (needed for SPE' parent dialogs)
    vector<PathID>              mBranchVector;					// Vector of branches that a node is apart of
    vector<PathID>              mTreeVector;					// Vector of trees that node is a part of (includes parent trees)
	map<string, DialogNode*>*	mChildMap;						// map <value, node> Children
	map<string, DialogNode*>*	mParentMap;						// map <value, node> Parent(s)

};

// End File
