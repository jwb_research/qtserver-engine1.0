/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "DialogNode.h"

DialogNode::Method DialogNode::String2Method(string eval_type) {
    std::transform(eval_type.begin(), eval_type.end(), eval_type.begin(), ::tolower);
    if (eval_type.compare("pe*") == 0) {
		cerr << "Console: Partial_Evaluation" << endl;
		return Method::Partial_Evaluation;
	}
    else if (eval_type.compare("spe'") == 0) {
		cerr << "Console: SingleArgument_Partial_Evaluation" << endl;
		return Method::SingleArgument_Partial_Evaluation;
	}
    else if (eval_type.compare("c") == 0) {
		cerr << "Console: Curry" << endl;
		return Method::Currying;
	}
    else if (eval_type.compare("i") == 0) {
		cerr << "Console: Interpretation" << endl;
		return Method::Interpretation;
	}
    else if (eval_type.compare("pfan") == 0) {
		cerr << "Console: Partial_Evaluation_Application" << endl;
		return Method::Partial_Evaluation_Application;
	}
	else {
		cerr << "Improper eval type: " << eval_type << endl;
	}
	abort();
	return Method::INVALID;
}

DialogNode::DialogNode(
    string nContextGrammar,
    string nValue,
    string nDetails,
    Method nEvaluationMethod,
    PathID nTreeID,
    PathID nTreePosition,
    bool nSubTree,
    bool nIsRoot,
    PathID nExitsNSubDialogs) :
    mSolicitation(new string("")),
    mValue(new string(nValue)),
    mContextGrammar(new string(nContextGrammar)),
    mDetails(new string(nDetails)),
    mEvaluationMethod(nEvaluationMethod),
    mSubTree(nSubTree),
    mLeafNode(true),
    mIsRoot(nIsRoot),
    mTreeID(nTreeID),
    mTreePosition(nTreePosition),
    mEndsNSubTrees(nExitsNSubDialogs),
    mChildMap(new map<string, DialogNode*>()),
    mParentMap(new map<string, DialogNode*>())
{

}

DialogNode::DialogNode(DialogNode& node) :
    mSolicitation(new string(node.GetSolicitation())),
    mValue(new string(node.GetValue())),
    mContextGrammar(new string(node.GetContextGrammar())),
    mDetails(new string(node.GetDetails())),
    mEvaluationMethod(node.GetEvaluationMethod()),
    mSubTree(node.IsSubTree()),
    mLeafNode(node.IsLeafNode()),
    mIsRoot(node.IsRoot()),
    mTreePosition(node.GetTreePosition()),
    mEndsNSubTrees(node.GetEndsNSubTrees()),
    mChildMap(new map<string, DialogNode*>()),
    mParentMap(new map<string, DialogNode*>())
{
	// Note does not copy parents or children!!
    // Does not copy tree ID!
}

DialogNode::~DialogNode()
{
	delete mSolicitation;
	delete mValue;
	delete mChildMap;
	delete mParentMap;
	delete mContextGrammar;
}

int DialogNode::GetNodesRemaining() {
    if (!this->IsLeafNode()) {
        int count = 1;
        DialogNode* node = this->mChildMap->begin()->second;
        while (!node->IsLeafNode()) {
            count++;
            node = node->mChildMap->begin()->second;
        }
        return count;
    }
    return 0;
}

bool DialogNode::IsSubTree() {
	return this->mSubTree;
}
bool DialogNode::IsLeafNode() {
	return this->mLeafNode;
}

bool DialogNode::IsRoot() {
    return this->mIsRoot;
}

string DialogNode::GetSolicitation() {
	return *this->mSolicitation;
}
string DialogNode::GetValue() {
	return *this->mValue;
}
string DialogNode::GetDetails() {
    return *this->mDetails;
}

long long DialogNode::GetTreeID() {
    return this->mTreeID;
}

string DialogNode::GetContextGrammar() {
	return *this->mContextGrammar;
}
DialogNode::Method DialogNode::GetEvaluationMethod() {
	return this->mEvaluationMethod;
}

DialogNode::PathID DialogNode::GetTreePosition() {
	return this->mTreePosition;
}

DialogNode::PathID DialogNode::GetEndsNSubTrees() {
	return this->mEndsNSubTrees;
}

// Return Child Nodes that are still valid responses
vector<DialogNode*> DialogNode::Children(vector<DialogNode::PathID> nBranchVector) {
    vector<DialogNode*> AvailableNodes;
    //transform(this->mChildMap->begin(), this->mChildMap->end(), back_inserter(choices), RetrieveKey());
    map<string, DialogNode*>::iterator it;
    for (it = this->mChildMap->begin(); it != this->mChildMap->end(); ++it) {
        if (it->second->CompareBranches(nBranchVector)) {
            AvailableNodes.push_back(it->second);
        }
    }
    return AvailableNodes;
}

void DialogNode::LinkToNode(DialogNode* node, Relationship relationship) {
	switch (relationship) {
	case Relationship::CHILD:
		this->mChildMap->insert(pair<string, DialogNode*>(node->GetValue(), node));
		node->mParentMap->insert(pair<string, DialogNode*>(this->GetValue(), this));
		if (node->mTreeID != this->mTreeID) {
			this->mTreeVector.push_back(node->mTreeID);
		}
        // std::cerr << "Added: " << node->GetValue() << " To: " << this->GetValue() << "| SUCCESS\n";
		break;
	case Relationship::PARENT:
		this->mParentMap->insert(pair<string, DialogNode*>(node->GetValue(), node));
		node->mChildMap->insert(pair<string, DialogNode*>(this->GetValue(), this));
		if (node->mTreeID != this->mTreeID) {
			node->mTreeVector.push_back(this->mTreeID);
		}
        // std::cerr << "Added: " << this->GetValue() << " To: " << node->GetValue() << "| SUCCESS\n";
		break;
	}
}

bool DialogNode::CompareBranches(vector<DialogNode::PathID> nCurrentBranches) {
    if (nCurrentBranches.size() == 0) { // No responses Yet, Every Choice is Valid
        return true;
    }
    // new branch is intersection
    vector<DialogNode::PathID> newBranch(nCurrentBranches.size());
    vector<DialogNode::PathID>::iterator it = set_intersection(this->mBranchVector.begin(), this->mBranchVector.end(),
        nCurrentBranches.begin(), nCurrentBranches.end(), newBranch.begin());
    newBranch.resize(it - newBranch.begin());
    if (newBranch.size() == 0) {
        return false;
    }

    return true;
}
