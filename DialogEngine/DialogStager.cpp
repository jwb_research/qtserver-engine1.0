/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the project nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "DialogStager.h"


DialogStager::DialogStager(Dialog* nDialog) :
mDialog(nDialog)
{
    mStagingData = NULL;
	if (nDialog != NULL) {
		mStagingData = new StagingData(nDialog->GetStartNodes().at(0));
	}
    else {
        cout << endl << "ERROR: failed to make stager because dialog was NULL." << endl;
        abort();
    }
}

DialogStager::~DialogStager()
{
    if (mStagingData) {
        delete mStagingData;
    }
}

int DialogStager::ExecuteDialog(bool showChoices) {
	string user_response;
	std::cerr << endl;
	std::cerr << "------------Entering Dialog-------------" << endl << endl;
	cout << this->GetStartPrompt() << endl;
	do {
		cout << GetSolicitation() << endl;
		cout << endl;
		if (showChoices) {
			cout << "----------------Choices-----------------" << endl;
            string choices = this->mStagingData->GetChoicesListAsString();
            cout << choices;
		}

		cout << endl;
		cout << "User Response: "; getline(cin, user_response);

        std::cerr << endl;
        std::cerr << "-----------------Console----------------" << endl;
        std::cerr << endl;
        string message = Evaluate(user_response); // Console Output
        std::cerr << "---------------End Console--------------" << endl;
        std::cerr << endl;
        std::cout << message << endl;

        std::cout << endl;
	} while (!this->mStagingData->IsFinished());

	std::cerr << "-------------Ending Dialog--------------" << endl << endl;

	return 0;
}

int DialogStager::GetQuestionsRemaining() {
    return this->mStagingData->GetQuestionsRemaining();
}

int DialogStager::GetNumberOfAnswers() {
    return this->mStagingData->GetNumberOfAnswers();
}

string DialogStager::Evaluate(string Utterance) {
	string  system_response = "TODO: Defualt: No reponse was provided. ";
    bool isQuery = false;

    if (Utterance.compare("/undo") == 0) {
        return this->Undo();
    }
    else if (Utterance.compare("/redo") == 0) {
        return this->Redo();
    }
    else if ((Utterance.compare("/reload") == 0) ||
             (Utterance.compare("/restart") == 0)) {
        return this->Restart();
    }
    // Utterance is a user query
    else if (Utterance.find("Q:") == 0) {
        isQuery = true;
    }

    // Get dialog keys by parsing the utterance
    // vector<string> dialogKeys = ParseUtterance(Utterance);
    vector<string> dialogKeys = this->mDialog->Tokenize(Utterance);

    if (!(dialogKeys.size() > 0)) { // No keywords detected
            return "Sorry, no keywords detected. Please reply to the prompt...";
    }

    // Show Answers:
    std::cerr << "Console: You Answered: " << endl << "     ";
    for (vector<string>::iterator it = dialogKeys.begin(); it != dialogKeys.end(); ++it) {
        std::cerr << *it << std::endl;
    }
    std::cerr << endl;

    // Process Utterance:
    DialogNode::Method result;

    if (isQuery) {
        result = this->mDialog->ProcessQuery(dialogKeys, this->mStagingData, system_response);
    }
    else {

        // Pushback CurrentData:
        this->PushBackStagingData();

        // Attempt to process and apply keys
        result = this->mDialog->ProcessDialogKeys(dialogKeys, this->mStagingData, system_response);

        // Manage Redo/Undo Stacks based on result.
        if (result == DialogNode::Method::INVALID) {
            this->PopBackStagingData(); // Don't save invalid undo history
        }
        else {
            this->ClearRedoHistory();
        }
    }

	return system_response;
}

vector<string> DialogStager::ParseUtterance(string user_response) {
	vector<string> Answers;

    string Key = "", LargerKey = "";
    char nextChar;
    std::vector<char> char_vec(user_response.rbegin(), user_response.rend());

	// Parse Dialog Tokens
    while (char_vec.size()) {
        nextChar = char_vec.back();
        char_vec.pop_back();
        Key = Key + nextChar;
        switch (this->mDialog->searchKeysFor(Key)){
		case (-1) :
            Key = "";

            // ignore following non spaces
            if (!isspace(nextChar)) {
                while (char_vec.size() && !isspace(char_vec.at(char_vec.size()-1))) {
//                    cerr << "Ignoring: " << char_vec.back() << endl;
                    char_vec.pop_back();
                    // INCRIMENT
                }
            }
			break;
		case (0) :
			// Need to check if longer is better match!
            LargerKey = Key;
            while (char_vec.size()) {
                nextChar = char_vec.back();
                char_vec.pop_back();
                LargerKey = LargerKey + nextChar;
                int i = this->mDialog->searchKeysFor(LargerKey);
				if (i < 0) {
                    if (!isspace(nextChar))
                    {
                        Key = ""; // Not a key only first part of word matched!
                    }
					break; // no longer match
				}
				else if (i==0) {
                    Key = LargerKey; // longer match
				}
                else {
                    if (!Key.size())
                    {
                        Key = ""; // Not a key only first part of word matched!
                    }
                }
			}
			// pushback longest match
            if (Key.size()) {
                Answers.push_back(Key);
                Key = "";
            }
            else {
                // ignore following non spaces
                if (!isspace(nextChar)) {
                    while (char_vec.size() && !isspace(char_vec.at(char_vec.size()-1))) {
    //                    cerr << "Ignoring: " << char_vec.back() << endl;
                        char_vec.pop_back();
                        // INCRIMENT
                    }
                }
            }
			break;
		default:

			break;
		}
	}

	return Answers;
}

string DialogStager::GetSolicitation(bool showChoices) {
    stringstream solicitationStrm;
    string solicitation = this->mStagingData->GetSolicitation();
    solicitationStrm << solicitation;
    if (showChoices && solicitation.size() > 0) {
        solicitationStrm << "\n" << this->mStagingData->GetChoicesListAsString();
    }
    return solicitationStrm.str(); // Must be empty string if dialog is done
}

string DialogStager::GetStartPrompt() {
	return this->mDialog->GetStartPrompt();
}

string DialogStager::Redo() {
	if (this->mStagingDataRedoStack.size() > 0) {
		this->mStagingDataUndoStack.push_back(this->mStagingData);
		this->mStagingData = this->mStagingDataRedoStack.at(this->mStagingDataRedoStack.size() - 1);
		this->mStagingDataRedoStack.pop_back();
		cerr << "Redo() : Success\n";
        return "Redo successful.";
	}
	else {
		cerr << "Redo() : Nothing to Redo\n";
        return "Nothing to redo.";
	}
}

string DialogStager::Undo() {
	if (this->mStagingDataUndoStack.size() > 0) {
		this->mStagingDataRedoStack.push_back(this->mStagingData);
		this->mStagingData = this->mStagingDataUndoStack.at(this->mStagingDataUndoStack.size() - 1);
		this->mStagingDataUndoStack.pop_back();
		cerr << "Undo() : Success\n";
        return "Undo successful.";
	}
	else {
		cerr << "Undo() : Nothing to Undo\n";
        return "Nothing to undo.";
	}
}

string DialogStager::Restart() {
    if (this->mStagingDataUndoStack.size() > 0) {
        while(this->mStagingDataUndoStack.size() > 0) {
            this->Undo();
        }
        ClearRedoHistory();
        cerr << "Reload() : Success\n";
        return "Reloaded";
    }
    cerr << "Reload() : Nothing to Reload\n";
    return "Nothing to Reload";
}

void DialogStager::PushBackStagingData() {
	if (this->mStagingDataUndoStack.size() < MAX_UNDO_SIZE) {
		this->mStagingDataUndoStack.push_back(this->mStagingData);
		this->mStagingData = new StagingData(this->mStagingData);
	}
	else { // Remove old UNDO data
		this->mStagingDataUndoStack.erase(this->mStagingDataUndoStack.begin());
		this->PushBackStagingData(); // Recursive Call
	}
}

void DialogStager::PopBackStagingData() {
	this->mStagingDataUndoStack.pop_back();
}

void DialogStager::ClearRedoHistory() {
	// Remove any REDO data
	for (StagingData* data : mStagingDataRedoStack) {
		delete data;
		data = NULL;
	}
	this->mStagingDataRedoStack.clear();
}
