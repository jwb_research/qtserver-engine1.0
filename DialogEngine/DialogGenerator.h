/*
 * Copyright (c) <2015->, <Joshua Buck>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once

#include "DialogInclude.h" //
#include "Dialog.h"         // Stores the Constructed Dialog
#include "Generation/PseudoDialog.h"   // Stores Temporary Dialog Metadata

/*
* Responsibility: Gnerate new dialogs from a specification
*
*
*
*/
class DialogGenerator {
public:
    // Normal way to create a Dialog by proving an XML-esque specification
	DialogGenerator(string XML_file);

	~DialogGenerator();

    static Dialog*  RunDemo(); // DEMO AND TESTING ONLY

    Dialog*         GetDialog();

private:

    enum XML_LEVEL {
        DIALOG,
        PROMPT,
        RESPONSE
    };

    // Dialog Building Blocks
    PseudoDialog*                   _dialog;
    PseudoPrompt*                   _prompt;
    PseudoNode*                     _node;
    map<std::string, PseudoDialog*> _dialog_map;

    // Copy constructor (PRIVATE) Do not use. Create a fresh dialog from an XML specification using the constructor
    DialogGenerator(DialogGenerator& _do_not_use);

    void BuildDialogsFromFile(string XMLfile);

    bool ParseXML(xml_node<> * first_node, XML_LEVEL level);

    void BuildExpressions(vector<string> &expressions);

    string GetEvaluation(string &expression);

    void BuildNodeChildren(string expression, vector<DialogNode *> ParentNodes,
                           PseudoDialog *pDialog, vector<long long> tree_vector);

    Dialog *                mDialog;
    long long				TREE_COUNTER;


};

