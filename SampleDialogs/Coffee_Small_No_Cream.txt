Coffee PE*(size::medium:large, blend::_any, cream::_any)
|      PE*(size::small, blend::_any, cream::no)

<Dialog name="Coffee" entry_prompt="Welcome to Flyer Coffee Machine">
	<Prompt name="Size" prompt="What size coffee would you like?">
		<Node name="small" />
		<Node name="medium" />
		<Node name="large" />
	</Prompt>
	<Prompt name="Blend" prompt="What blend of coffee would you like?">
		<Node name="light" />
		<Node name="dark" />
	</Prompt>
	<Prompt name="Cream" prompt="Would you like cream in your coffee?">
		<Node name="Yes" />
		<Node name="No" />
	</Prompt>
</Dialog>

