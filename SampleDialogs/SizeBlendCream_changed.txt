<Dialog entry_prompt="Welcome to the Flyer Coffee Machine." affirmative_response="Ok, " negative_response="Sorry, try again. ">
    <Node eval="PE*" value="root" prompt="What size coffee would you like? Small, Medium, or Large?" context="">
        <Node value="Small" prompt="What blend of coffee would you like? Light or Dark? " context="">
            <Node value="Light" prompt="Would you like to save room for cream? Yes or No? " context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
            <Node value="Dark" prompt="Would you like to save room for cream? Yes or No?" context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
        </Node>
        <Node value="Medium" prompt="What blend of coffee would you like? Light or Dark? " context="">
            <Node value="Light" prompt="Would you like to save room for cream? Yes or No? " context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
            <Node value="Dark" prompt="Would you like to save room for cream? Yes or No?" context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
        </Node>
        <Node value="Large" prompt="What blend of coffee would you like? Light or Dark? " context="">
            <Node value="Light" prompt="Would you like to save room for cream? Yes or No? " context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
            <Node value="Dark" prompt="Would you like to save room for cream? Yes or No?" context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
        </Node>
        <Node value="S" prompt="What blend of coffee would you like? Light or Dark? " context="">
            <Node value="Light" prompt="Would you like to save room for cream? Yes or No? " context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
            <Node value="Dark" prompt="Would you like to save room for cream? Yes or No?" context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
        </Node>
        <Node value="M" prompt="What blend of coffee would you like? Light or Dark? " context="">
            <Node value="Light" prompt="Would you like to save room for cream? Yes or No? " context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
            <Node value="Dark" prompt="Would you like to save room for cream? Yes or No?" context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
        </Node>
        <Node value="L" prompt="What blend of coffee would you like? Light or Dark? " context="">
            <Node value="Light" prompt="Would you like to save room for cream? Yes or No? " context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
            <Node value="Dark" prompt="Would you like to save room for cream? Yes or No?" context="">
                <Node value="Yes" prompt="" context=""/>
                <Node value="No" prompt="" context=""/>
                <Node value="Y" prompt="" context=""/>
                <Node value="N" prompt="" context=""/>
            </Node>
        </Node>
    </Node>
</Dialog>
