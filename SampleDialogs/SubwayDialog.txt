Subway PE*(Menu_Choice::Sub, Sub_Size::_any, Bread::_any, Cheese::_any, Toppings::_any, Sauce::_any, takeout::_any)
|      PE*(Menu_Choice::Salad)

<Dialog name="Subway" entry_prompt="Welcome to Subway">
	<Prompt name="Menu_Choice" prompt="What would you like? Sub or Salad?">
		<Node name="Sub" />
		<Node name="Salad" />
	</Prompt>
	<Prompt name="Sub_Size" prompt="What size sub would you like? 6inch or 12inch?">
		<Node name="six inch" />
		<Node name="twelve inch" />
	</Prompt>
	<Prompt name="Bread" prompt="What type of bread would you like?">
		<Node name="Italian" />
		<Node name="Wheat" />
		<Node name="Honey Oat" />
		<Node name="Flatbread" />
		<Node name="Herb & Cheese" />
	</Prompt>
	<Prompt name="Cheese" prompt="What kind of cheese would you like?">
		<Node name="American" />
		<Node name="Mozzarella" />
		<Node name="Cheddar" />
	</Prompt>
	<Prompt name="Toppings" prompt="What toppings would you like?">
		<Node name="Pickles" />
		<Node name="olives" />
		<Node name="banana peppers" />
		<Node name="jalapeños" />
		<Node name="spinach" />
		<Node name="red onion" />
		<Node name="tomatoes" />
		<Node name="lettuce" />
		<Node name="peppers" />
		<Node name="cucumbers" />
	</Prompt>
	<Prompt name="Sauce" prompt="What sauce would you like?">
		<Node name="mustard" />
		<Node name="sweet onion" />
		<Node name="mayo" />
		<Node name="oil" />
		<Node name="light mayo" />
		<Node name="ranch" />
		<Node name="red wine vinegar" />
		<Node name="chipotle southwest" />
	</Prompt>
	<Prompt name="takeout" prompt="For here or to go?">
		<Node name="to go" />
		<Node name="for here" />
	</Prompt>
</Dialog>