# CHANGELOG #

Last Revised: 11 February 2017

##<a name="contents"></a>Contents

###[2017](#2017)

3. [February 2017](#Feb17)

3. [January 2017](#Jan17)

###[2016](#2016)

2. [May 2016](#May16)

2. [April 2016](#April16)

3. [March 2016](#March16)

3. [February 2016](#Feb16)

3. [January 2016](#Jan16)

###[2015](#2015)

### [Comments](#Comments)

***

<a name="2017"></a>
#2017 Development

<a name="Feb17"></a>
### Feb 2017 ###

* Added NLPU - Natural Language Processing Unit

<a name="Jan17"></a>
### Jan 2017 ###

***

<a name="2016"></a>
#2016 Development

<a name="May16"></a>
### May 2016 ###

* Add User Queries for dialog elements
* Add User Queries for dialog element attributes
* Change Generation to support “U” eval strategies

<a name="April16"></a>
### April 2016 ###

* Added README
* Added CHANGLOG
* Restructured repo for distribution
* Fixed dialog server bugs
* Added ServerData class
* Added ServerCommands class
* Added “/shutdown”
* Changed “/engine on” and “/engine reload” to accept a file
* Removed dialog generation file location dependency
* Added evaluation implication (implied unsolicited responses)
* Added skeleton for user queries with "Q:"

<a name="March16"></a>
### March 2016 ###

* Fixed evaluation bugs
* Added QT support
* Added SFML networking in QT
* Created QT server console
* Greatly improved server design

<a name="Feb16"></a>
### Feb 2016 ###

* Added PFAn evaluation
* Added I    evaluation
* Added SPE’ evaluation
* Added sub-dialogs
* Changed generation procedure
* Fixed generation bugs

<a name="Jan16"></a>
### Jan 2016 ###

* Created repo
* Added SFML networking to create server
* Added Engine 1.0 to server 
* Added “/help”
* Added “/engine on | off”
* Added “/engine reload”
* Added “/say”
* Added “/safespeak on | off”

***

<a name="2015"></a>
#2015 Development

* Research into dialog data representation
* Design of initial dialog engine
* Added C   evaluation
* Added PE* evaluation
* Created TestEngine1.0

<a name="Comments"></a>
### Comments ###

I am actively working concurrently on several enhancements to the dialog toolkit. Some of these enhancements are small (marked by ‘+’) and some are very large (marked by ‘++…’). I will try to break them down into categories and describe what they do. 

Evaluation Enhancements: (!!! main priority !!!)


+++Add functionality to support node ‘attribute’ called details
	Allows the user to query ‘attributes’ of nodes such as the time of a flight or what requirements a class satisfies
	i.e., user can say “I need classes that satisfy Natural Science Requirement” and the engine will remove possible responses that do not satisfy this requirement. This is basically filtering possible responses based on details about the responses

COMPLETE 4/25/16 
++Add functionality to automatically infer responses 
	i.e., infer [Department=Computer Science] and [Undergraduate=true] when user indicates [CPS 150]

Generation Enhancements:

+Add additional error handling for improper XML specification

COMPLETE 4/20/16 
+Add details attribute to XML nodes

COMPLETE 4/20/16
+Permit some XML attributes to be optional instead of required (only value is required)

Staging Enhancements:

COMPLETE 4/20/16
+print details (attributes) of possible responses such as the time information for courses or flights or what requirements a course satisfies
	Note that these details can be queried by the user and evaluated.
 
+write a parser for detecting user queries 
	will eventually need to use an advanced algorithm (confabulation) for determining the best query to execute based on incomplete or arbitrary user queries

Client UI Enhancements:

COMPLETE 4/15/16
+Allow user to view session message history by scrolling up (scroll bar defaults to maximum value)

COMPLETE 4/15/16
+Allow user to hit enter/return key to send instead of pushing the send button

++Add user survey (SUS) after dialog finishes for user study.

Server Enhancements:

+create a log for all user session data and survey results

+support additional server commands: shutdown, kick, reload [user] [all], say [user], clear_screen [user] [all], etc. 

Dialog Designer UI:

++++Research and design dialog designer UI (may use as CPS 499 final project)

Other:

++Configure toolkit for release

——————————————————————————————————————————————————————————————————————


Known Issues: // Might be solved now
	- Generation does not protect sub-dialog generation
		*Generator will build dialog subtrees with parents that are not C or SPE' but the stager will not evaluate correctly. 
		*Only parent dialogs that are of type C or SPE' are supported bu the Stager.
		Resolution: Need to prevent creating sub-dialogs when the parent dialog is not C or SPE'

	- For loops in I and PFAn evaluation functions are inefficient and could be removed with sufficient overhead
		*Need to Keep track of (sub-)dialog sizes in order to know if I is evaluated correctly.
		*May not be a better way to evaluate PFAn? 

Additions:
	-Method to specify dialogs without XML and then generate the XML file automatically

	-Unions of dialogs need to be implemented fully
		*Generator supports but stager does not evaluate them correctly.
