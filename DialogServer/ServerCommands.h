// This is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#ifndef SERVERCOMMANDS_H
#define SERVERCOMMANDS_H

#include <QDebug>
#include <vector>
#include <iomanip>
#include <map>

#include <QThread>
#include <QTcpSocket>
#include <QDataStream>

#include "ServerData.h"

using namespace std;

class ServerCommands : public QThread
{
    Q_OBJECT
public:
    ServerCommands(ServerData& nData);

    void run() Q_DECL_OVERRIDE;

signals:
    void error(QTcpSocket::SocketError socketError);

private:
    //    typedef function<void(string, ServerData*)> CommandTemplate; // Old Definition
    typedef void (*CommandTemplate)(string, ServerData*); // New Definition

    ServerData                              mServerData;
    map<string, CommandTemplate>            mCommandMap;

    bool ExecuteIfCommand(string command);

};

#endif // SERVERCOMMANDS_H
