// This file is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#include <QCoreApplication>

#include <QObject>

#include "DialogTCPServer.h"

int main(int argc, char *argv[])
{
    // Pass an XML specification file to test the engine without the server/client
    if (argc == 2) {
        string dialog_path = argv[1];

        DialogGenerator * gen = new DialogGenerator(dialog_path);

        DialogStager * stager = new DialogStager(gen->GetDialog());

        stager->ExecuteDialog(true); // or false if you don't want to see choices in std::out

        return 0;
    }

    // Run the normal server/client
    QCoreApplication a(argc, argv);

    DialogTCPServer Server(&a);

    Server.StartListen();

    QObject::connect(&Server, SIGNAL(finished()), &a, SLOT(quit()));

    return a.exec();
}
