// This is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#ifndef DIALOGTCPSERVER_H
#define DIALOGTCPSERVER_H

#include <QTcpServer>

#include "ServerThread.h"
#include "ServerCommands.h"

class DialogTCPServer : public QTcpServer
{
    Q_OBJECT

public:
    DialogTCPServer(QObject *parent = 0);
    ~DialogTCPServer();

    void StartListen();

signals:
    void finished();

protected:
    void incomingConnection(qintptr socketDescriptor) Q_DECL_OVERRIDE;

private:
    ServerCommands *mServerCommands;
    ServerData mServerData;

public slots:
    void ShutDown();
};

#endif // DIALOGTCPSERVER_H
