// This file is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#include "ServerData.h"

class ServerDataData : public QSharedData
{
public:
    ServerDataData() :
        mDialogueEngineOn(false),
        mSafeSpeakOn(true),
        mShutdownSignal(false),
        ServerAgentName("Agent")
    {

    }

    typedef std::map<std::string, DialogGenerator*> GenMap;

    GenMap                  Generators;                 // Generators
    bool                    mDialogueEngineOn;
    bool                    mSafeSpeakOn;
    bool                    mShutdownSignal;

    const std::string       ServerAgentName;        // Name of the agent that comunicates with users
};

ServerData::ServerData() : data(new ServerDataData){}
ServerData::ServerData(const ServerData &rhs) : data(rhs.data){}
ServerData &ServerData::operator=(const ServerData &rhs)
{
    if (this != &rhs)
        data.operator=(rhs.data);
    return *this;
}

ServerData::~ServerData()
{

}

vector<string> ServerData::Dialogs() {
    vector<string> dialogs;
    map<std::string, DialogGenerator*>::iterator it;
    for(it = this->data->Generators.begin(); it != this->data->Generators.end(); ++it) {
      dialogs.push_back(it->first);
    }
    return dialogs;
}

DialogStager* ServerData::CreateStager(std::string DialogName) {
    ServerDataData::GenMap::iterator it = data->Generators.find(DialogName);
    if (it != data->Generators.end()) {
        return new DialogStager(it->second->GetDialog());
    }
    return NULL;
}

void ServerData::AddGenerator(std::pair<string, DialogGenerator *> *gen) {
    this->data->Generators.insert(*gen);
}

bool ServerData::GetShutDownStatus() {return data->mShutdownSignal; }
void ServerData::SetShutDownStatus() {data->mShutdownSignal = true;}
bool ServerData::GetSafeSpeakStatus() {return data->mSafeSpeakOn;}
void ServerData::SetSafeSpeakStatus(bool status) {data->mSafeSpeakOn = status;}
