QT       += core
QT       += network
QT       -= gui

CONFIG += c++11

TARGET = DialogServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    ../DialogEngine/Dialog.cpp \
    ../DialogEngine/DialogGenerator.cpp \
    ../DialogEngine/DialogNode.cpp \
    ../DialogEngine/DialogStager.cpp \
    ../DialogEngine/Staging/StagingData.cpp \
    ../DialogEngine/DialogInclude.cpp \
    ServerThread.cpp \
    DialogTCPServer.cpp \
    ServerData.cpp \
    ServerCommands.cpp \
    ../DialogEngine/Generation/PseudoNode.cpp \
    ../DialogEngine/Generation/PseudoPrompt.cpp \
    ../DialogEngine/Generation/PseudoDialog.cpp \
    ../DialogEngine/NLPU/NLPUDictionary.cpp \
    ../DialogEngine/MyThes-1.0/mythes.cxx \
    ../DialogEngine/NLPU/NLPUThesaurusLookup.cpp

HEADERS += \
    ../DialogEngine/rapidxml-1.13/rapidxml.hpp \
    ../DialogEngine/rapidxml-1.13/rapidxml_iterators.hpp \
    ../DialogEngine/rapidxml-1.13/rapidxml_print.hpp \
    ../DialogEngine/rapidxml-1.13/rapidxml_utils.hpp \
    ../DialogEngine/Dialog.h \
    ../DialogEngine/DialogGenerator.h \
    ../DialogEngine/DialogNode.h \
    ../DialogEngine/DialogStager.h \
    ../DialogEngine/DialogInclude.h \
    ../DialogEngine/Staging/StagingData.h \
    ServerThread.h \
    DialogTCPServer.h \
    ServerData.h \
    ServerCommands.h \
    ../DialogEngine/Generation/PseudoNode.h \
    ../DialogEngine/Generation/PseudoPrompt.h \
    ../DialogEngine/Generation/PseudoDialog.h \
    ../DialogEngine/NLPU/NLPUDictionary.h \
    ../DialogEngine/MyThes-1.0/mythes.hxx \
    ../DialogEngine/NLPU/NLPUThesaurusLookup.h

DISTFILES += \
    ../DialogEngine/rapidxml-1.13/manual.html \
    ../DialogEngine/rapidxml-1.13/license.txt \
    ../DialogEngine/MyThes-1.0/th_en_US_new.dat \
    ../DialogEngine/MyThes-1.0/th_en_US_new.idx
