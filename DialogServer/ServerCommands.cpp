// This file is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#include "ServerCommands.h"

static void HelloWorld(string command, ServerData* nData = NULL) {
    if (nData != NULL) {
        cout << command << endl;
    }
    else {
        cout << "nData is null..." << endl;
    }
}

//static void Say(string command, ServerData* nData) {
//    string message = command.erase(0, 4);
//    cout << "SERVER: " << message << endl;
//    nData->BroadCast(SERVER_MSG, "SERVER: " + message);
//}

static void Help(string command, ServerData* nData) {
    cout << "Commands: " << command << endl;
    int indent = 22;
    cout << setw(indent) << left << "/help" << "Show list of commands" << endl;
//    cout << setw(indent) << left << "/say" << "Broadcast message to all users" << endl;
    cout << setw(indent) << left << "/safespeak on|off" << "Require /say to send messages" << endl;
    cout << setw(indent) << left << "/engine add <file>" << "Add a dialog that the client can choose" << endl;
//    cout << setw(indent) << left << "/engine reload <file>" << "Reload the dialogue engine" << endl;
    cout << setw(indent) << left << "/shutdown" << "Shutdown the server" << endl;
    nData->GetShutDownStatus(); // just needed to use nData to silence warning.
}

static void SafeSpeakOn(string command, ServerData* nData) {
    nData->SetSafeSpeakStatus(true);
    cout << "Event: " << command << endl;
}

static void SafeSpeakOff(string command, ServerData* nData) {
    nData->SetSafeSpeakStatus(false);
    cout << "Event: " << command << endl;
}

static void EngineAddDialog(string command, ServerData* nData) {
    cout << "Event: " << command << endl;
    string file = command.erase(0, 12);

    // Remove whitespace from end of string "file"
    int num_spaces = 0;
    for (string::reverse_iterator it = file.rbegin(); it != file.rend(); ++it) {
        if (*it != ' ') { break; }
        num_spaces++;
    }

    // Get File Path
    file = file.erase(file.size()-num_spaces, file.size());

    // Get Name
    std::string s = file;
    std::string delimiter = "/";

    size_t pos = 0;
    std::vector<string> token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token.push_back(s.substr(0, pos));
        s.erase(0, pos + delimiter.length());
    }
    // s contains what is left!

    // Add new Generators
    std::pair<string, DialogGenerator*>* newGenerator = new std::pair<string, DialogGenerator*>();
    newGenerator->first = s.substr(0, s.size() - 4);
    cout << "Attempting to open file \"" << file << "\"" << endl;
    newGenerator->second = new DialogGenerator(file);
    cout << "Successfully opened file \"" << file << "\"" << endl;
    nData->AddGenerator(newGenerator);
    newGenerator = NULL; // Do not delete data

//    else {
//        cout << "Event: Dialogue Engine On: Dialogue Engine is already on..." << endl;
//        cout << "       Try \"/engine reload\" to restart the dialog" << endl;
//    }
}

//static void EngineOff(string command, ServerData* nData) {
//    nData->mDialogueEngineOn = false;
//    cout << "Event: " << command << endl;
//    // Erase Existing Stagers
//    ServerData::ClientStagerMap::iterator it_stager;
//    for (it_stager = nData->mClientStagers.begin();
//         it_stager != nData->mClientStagers.end(); ++it_stager) {
//        delete it_stager->second;
//        it_stager->second = NULL;
//    }
//    nData->mClientStagers.clear();
//}

//static void EngineReload(string command, ServerData* nData) {
//    if (nData->mDialogueEngineOn) {
//        cout << "Event: " << command << endl;
//        string file = command.erase(0, 15);
//        // Remove whitespace from end of string "file"
//        int num_spaces = 0;
//        for (string::reverse_iterator it = file.rbegin(); it != file.rend(); ++it) {
//            if (*it != ' ') { break; }
//            num_spaces++;
//        }
//        file = file.erase(file.size()-num_spaces, file.size());
//        cout << "Attempting to open file \"" << file << "\"" << endl;
//        delete nData->mGenerator;
//        nData->mGenerator = new DialogGenerator(file);
//        cout << "Successfully opened file \"" << file << "\"" << endl;
//        // Erase Existing Stagers
//        ServerData::ClientStagerMap::iterator it_stager;
//        for (it_stager = nData->mClientStagers.begin();
//             it_stager != nData->mClientStagers.end(); ++it_stager) {
//            delete it_stager->second;
//            it_stager->second = NULL;
//        }
//        nData->mClientStagers.clear();
//        // Add new Stagers
//        ServerData::ClientsMap::iterator it_client;
//        for (it_client=nData->mClients.begin(); it_client!=nData->mClients.end(); ++it_client) {
//            nData->InitStagerForClient(&it_client);
//        }
//    }
//    else {
//        cout << "Event: Dialogue Engine Reload: Turn Dialogue Engine on first..." << endl;
//        cout << "       Try \"/engine on\" to restart the dialog" << endl;
//    }
//}

static void ShutDown(string command, ServerData* nData) {
    cout << "Event: " << command << endl;
    nData->SetShutDownStatus();
}

// Add commands to server that are executed on mServerData(nData)
ServerCommands::ServerCommands(ServerData &nData) :
    mServerData(nData)
{
    //               -"/commandname"-           -Function-
    this->mCommandMap["/hello world"] =         HelloWorld;
    this->mCommandMap["/help"] =                Help;
//    this->mCommandMap["/say "] =                Say;
    this->mCommandMap["/safespeak on"] =        SafeSpeakOn;
    this->mCommandMap["/safespeak off"] =       SafeSpeakOff;
    this->mCommandMap["/engine add "] =         EngineAddDialog;
//    this->mCommandMap["/engine off"] =          EngineOff;
//    this->mCommandMap["/engine reload "] =      EngineReload;
    this->mCommandMap["/shutdown"] =            ShutDown;
}

void ServerCommands::run() {
    std::string s;
    qDebug() << "Started Listening for Commands.";
    while(!this->mServerData.GetShutDownStatus())
    {
        std::getline(std::cin, s);
        this->ExecuteIfCommand(s); // TODO: returns bool
    }
    qDebug() << "...Command Listening Stopped.";
}


// Function to execute a server command on mServerData
bool ServerCommands::ExecuteIfCommand(string command) {
    if (command.size() < 1) {
        return false;
    }
    if (command.at(0) == '/') {
        map<string, CommandTemplate>::iterator it;
        // TODO: mCommandMap is already sorted... probably a faster way to lookup command!
        for (it = this->mCommandMap.begin(); it != this->mCommandMap.end(); ++it) {
            if (command.find(it->first) == 0) {
                it->second(command, &this->mServerData); // This calls the fucntion stored in it->second
                return true;
            }
        }
        cout << "Invalid Command: \"" << command << "\"" << endl;
        cout << "Use \"/help\" for available commands." << endl;
        return false;
    }
    // If message is not a command and safespeak is off, broadcast message to all users
    else if (!this->mServerData.GetSafeSpeakStatus()) {
        cout << "SERVER: " << command << endl;
//        this->mServerData->BroadCast(SERVER_MSG, "SERVER: " + command);
    }
    else {
        cout << "Safespeak is ON so message \"" << command << "\" was not sent." << endl;
        cout << "Use \"/help\" for available commands." << endl;
    }

    // Returns wether or not something was done.
    return this->mServerData.GetSafeSpeakStatus();
}
