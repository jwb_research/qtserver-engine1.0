// This file is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#ifndef SERVERTHREAD_H
#define SERVERTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QDataStream>
#include <QBuffer>

#include "ServerData.h"

typedef int PacketType;
const PacketType INIT_NEW_CLIENT=0;
const PacketType GENERAL_MSG=1;
const PacketType SERVER_MSG=2;
const PacketType COMMAND_MSG=3;
const PacketType PROGRESS_MSG=4;
const PacketType DIALOG_MSG=5;

class ServerThread : public QThread
{
    Q_OBJECT

public:
    ServerThread(int socketDescriptor, ServerData &generators, QObject *parent);

    void run() Q_DECL_OVERRIDE;

signals:
    void error(QTcpSocket::SocketError socketError);

private:
    int                 SocketDescriptor;
    QTcpSocket          mTcpSocket;
    ServerData          mServerData;
    DialogStager*       mStager;
    std::string         mClientName;

    void Receive();
    void Send(PacketType type, std::string Message);
    void ProcessMessage(QString Message);

};

#endif // SERVERTHREAD_H
