#ifndef SERVERDATA_H
#define SERVERDATA_H

#include <QObject>
#include <QSharedDataPointer>
#include <QExplicitlySharedDataPointer>

#include "../DialogEngine/DialogGenerator.h"
#include "../DialogEngine/DialogStager.h"

class ServerDataData;

class ServerData
{
public:
    ServerData();
    ServerData(const ServerData &);
    ServerData &operator=(const ServerData &);
    ~ServerData();

    bool GetShutDownStatus();
    void SetShutDownStatus();
    bool GetSafeSpeakStatus();
    void SetSafeSpeakStatus(bool status);

    vector<string> Dialogs();
    void AddGenerator(std::pair<string, DialogGenerator*>* gen);
    DialogStager* CreateStager(std::string DialogName);

private:
    QExplicitlySharedDataPointer<ServerDataData> data;
};

#endif // SERVERDATA_H
