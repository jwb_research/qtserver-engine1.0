// This file is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#include "DialogTCPServer.h"

DialogTCPServer::DialogTCPServer(QObject *parent) :
    QTcpServer(parent)
{
    mServerCommands = new ServerCommands(this->mServerData); // start after listen

    connect(mServerCommands, SIGNAL(finished()), this, SLOT(ShutDown()));
    connect(mServerCommands, SIGNAL(finished()), mServerCommands, SLOT(deleteLater()));
}

DialogTCPServer::~DialogTCPServer()
{
}

void DialogTCPServer::ShutDown() {
    qDebug() << "...TCP Listening Stopped.";
    delete mServerCommands;
    // TODO: Wait for Clients to die
    emit finished();
    this->close();
}

void DialogTCPServer::StartListen() {
    // Try to open the port
    // Ask Admin for Port Number
    unsigned short port;
    qDebug()<<"What port?";
    std::cin>>port;
    while (!this->listen(QHostAddress::Any, (quint16)port)) {
        qDebug() << "cannot listen to port";
        qDebug() << "try again (y/n) ? ";
        char r; std::cin >> r;
        if (r == 'n') { abort(); }
        else {
            // Ask Admin for Port Number
            qDebug()<<"What port? ";
            std::cin>>port;
        }
    }
    qDebug()<<"Connected!";
    mServerCommands->start();
}

void DialogTCPServer::incomingConnection(qintptr socketDescriptor)
{
    ServerThread *thread = new ServerThread(socketDescriptor, mServerData, this);
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(mServerCommands, SIGNAL(finished()), thread, SLOT(terminate()));
    qDebug()<<"incomingConnection()!";
    thread->start();
}
