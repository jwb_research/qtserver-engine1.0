// This file is free, opensource code
// No license or copyright applicable to this file. See Qt's terms of use.

#include "ServerThread.h"

ServerThread::ServerThread(int socketDescriptor, ServerData &data, QObject *parent)
    : QThread(parent),
      SocketDescriptor(socketDescriptor),
      mServerData(data),
      mStager(NULL)
{

}

void ServerThread::run()
{
    if (!mTcpSocket.setSocketDescriptor(SocketDescriptor)) {
        emit error(mTcpSocket.error());
        return;
    }

    while (!this->mServerData.GetShutDownStatus() && mTcpSocket.isOpen()) {
        if (mTcpSocket.state() != QAbstractSocket::ConnectedState) {
            std::cout << "Event: " << this->mClientName << " has disconnected.\n";
            return;
        }
        if (mTcpSocket.waitForReadyRead(60000)) {
            this->Receive();
        }
    }
    std::cout << "Event: Server disconnecting " << this->mClientName << ".\n";
    mTcpSocket.disconnectFromHost();
    mTcpSocket.waitForDisconnected();

    //TODO: Remove client data
    if (this->mStager) {
        delete this->mStager;
    }
}

void ServerThread::Send(PacketType type, std::string Message) {
    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QDataStream out(&buffer);
    out << std::to_string(type).append("_$Token$_").append(Message).c_str();
    if (!this->mTcpSocket.write(buffer.data())) {
        qDebug() << "DEBUG: Failed to send message: " << Message.c_str();
        return;
    }
    mTcpSocket.waitForBytesWritten();
}

void ServerThread::Receive()
{
    int dataSize;
    while(mTcpSocket.read((char*)&dataSize, sizeof(int)))
    {
        QByteArray buffer = mTcpSocket.read(dataSize);
        QString Message(buffer);
        this->ProcessMessage(Message);
    }
}

void ServerThread::ProcessMessage(QString Message)
{
    QStringList Tokens = Message.split("_$Token$_");
    if (Tokens.size() < 2) {
        qDebug() << "DEBUG: Improper Message: " << Message;
        return;
    }
    int type = std::stoi((Tokens.at(0)).toStdString());
    if(type==INIT_NEW_CLIENT)
    {
        if (Tokens.size() != 2) {
            qDebug() << "DEBUG: Improper INIT_NEW_CLIENT: " << Message;
            return; // TODO disconnect bad client?
        }
        this->mClientName = (Tokens.at(1)).toStdString();
        std::cout << "Event: " << this->mClientName << " has joined.\n";
        for (string dialogName : this->mServerData.Dialogs()) {
            this->Send(DIALOG_MSG, dialogName);
        }
    }
    else if (type==DIALOG_MSG) {
        cout << "Token Dialog = " << (Tokens.at(1)).toStdString() << endl;
        DialogStager* newStager = this->mServerData.CreateStager((Tokens.at(1)).toStdString());
        if (newStager) {
            this->mStager = newStager;
            this->Send(GENERAL_MSG, this->mStager->GetStartPrompt());
            this->Send(GENERAL_MSG, this->mStager->GetSolicitation());
        }
    }
    else if(type==GENERAL_MSG)
    {
        std::string msg = (Tokens.at(1)).toStdString();
        std::cout << "\t" << this->mClientName << ": "<<msg<<"\n";
        if (this->mStager) {
            std::transform(msg.begin(), msg.end(), msg.begin(), ::tolower); // TODO: Make Option
            this->Send(GENERAL_MSG, this->mStager->Evaluate(msg));
            string NextSolicitation = this->mStager->GetSolicitation();
            if (NextSolicitation.length() > 0) {
                this->Send(GENERAL_MSG, NextSolicitation);
            }
            else {
                // Dialog Finished
                this->Send(GENERAL_MSG, "Dialog Complete.");
                delete this->mStager; // TODO: allow reload?
                this->mStager = NULL;
            }

        }
    }
}
